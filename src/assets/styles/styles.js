const styles = {
    serviceCardHeader: {
        borderBottom: 'none'
    },
    steps:{
        marginTop: 120,
        marginBottom: 20,
    }
};

export default styles;