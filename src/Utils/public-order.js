export const APP_PUBLIC_ORDER_KEY = "@venturawebapp-AppPublicOrderKey";
export const hasOrder = () => localStorage.getItem(APP_PUBLIC_ORDER_KEY) !== null;

export const getOrderHash = () => {
  var output = null, currentOrder = getOrder();
  if (currentOrder !== null) {
    output = currentOrder.order_hash;
  }
  return output;
};

export const getOrderId = () => {
  var output = null, currentOrder = getOrder();
  if (currentOrder !== null) {
    output = currentOrder.id;
  }
  return output;
};

export const getOrder = () => {
  var output = null, rawOrder = localStorage.getItem(APP_PUBLIC_ORDER_KEY);
  if (rawOrder !== null) {
    var currentOrder = JSON.parse(rawOrder);
    output = currentOrder;
  }
  return output;
};

export const registerNewOrder = orderInfo => {
  var orderToSave = JSON.stringify(orderInfo);
  localStorage.setItem(APP_PUBLIC_ORDER_KEY, orderToSave);
};

export const clearLocalCurrentOrder = () => {
  localStorage.removeItem(APP_PUBLIC_ORDER_KEY);
};
