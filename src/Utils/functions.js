import { message as feedbackMessage } from 'antd';

export const handleResultData = (result) => {
    //Adicionando a o campo 'key' pra permitir a renderização da tabela sem erros
    //Adicionando a o campo 'index' pra permitir a ordenação da tabela (caso necessário)
    if (typeof (result.data) !== 'undefined' && result.data.length > 0) {
        for (var i = 0; i < result.data.length; i++) {
            result.data[i].key = result.data[i].id;
            result.data[i].index = i;
        }
    }
    return result;
}

export const flash = (actionFeedbackMessage) => {
    let { type, message, duration } = actionFeedbackMessage;

    if (typeof (duration) === 'undefined') {
        duration = 5;
    }

    switch (type) {
        case 'warning': {
            feedbackMessage.warning(message, duration);
        } break;
        case 'success': {
            feedbackMessage.success(message, duration);
        } break;
        case 'error': {
            feedbackMessage.error(message, duration);
        } break;
        case 'info':
        default: {
            feedbackMessage.info(message, duration);
        } break;
    }
}

export const currencyFormatter = value => {
    return new Intl.NumberFormat('pt-br', {
        style: "currency",
        currency: "BRL"
    }).format(value);
};

export const currencyParser = val => {
    try {
        // for when the input gets clears
        if (typeof val === "string" && !val.length) {
            val = "0.00";
        }

        // detecting and parsing between comma and dot
        var group = new Intl.NumberFormat('pt-br').format(1111).replace(/1/g, "");
        var decimal = new Intl.NumberFormat('pt-br').format(1.1).replace(/1/g, "");
        var reversedVal = val.replace(new RegExp("\\" + group, "g"), "");
        reversedVal = reversedVal.replace(new RegExp("\\" + decimal, "g"), ".");
        //  => 1232.21 €

        // removing everything except the digits and dot
        reversedVal = reversedVal.replace(/[^0-9.]/g, "");
        //  => 1232.21

        // appending digits properly
        const digitsAfterDecimalCount = (reversedVal.split(".")[1] || []).length;
        const needsDigitsAppended = digitsAfterDecimalCount > 2;

        if (needsDigitsAppended) {
            reversedVal = reversedVal * Math.pow(10, digitsAfterDecimalCount - 2);
        }

        return Number.isNaN(reversedVal) ? 0 : reversedVal;
    } catch (error) {
        console.error(error);
    }
};

export const validatePhoneInput = (event) => {
    let { value } = event.target

    value = value.replace(/\D/g, '')
    value = value.replace(/(\d{2})/, '($1) ')
    const isMobilePhone = /^[(][0-9][0-9][)][\s][9]/.test(value)

    if (!/[0-9]/.test(event.key)) {
        event.preventDefault()
    }

    if (isMobilePhone) {
        event.target.maxLength = 16
        value = value.replace(/\D/g, '')
        value = value.replace(/(\d{2})(\d{1})/, '($1) $2 ')
        value = value.replace(/(\d{4})/, '$1-')
        value = value.replace(/(\d{4})/, '$1')
    } else {
        event.target.maxLength = 14
        value = value.replace(/(\d{4})/, '$1-')
        value = value.replace(/(\d{4})/, '$1')
    }
    
    event.target.value = value
}

export const functions = [
    handleResultData,
    flash
];

export default functions;