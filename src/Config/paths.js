// As rotas estão sendo definidas aqui pois o component
// Components/Breadcrumb.js utiliza esse arquivo para descobrir qual é o título da página


//Ícones disponíveis
import { HomeOutlined } from '@ant-design/icons';

//- Páginas públicas
import PublicMain from 'Pages/Public/Main';
import BasicCustomerForm from 'Pages/Public/Orders/BasicCustomerForm';
import PlanBudgetSelection from 'Pages/Public/Orders/PlanBudgetSelection';
import CustomerCreateCustomOrder from 'Pages/Public/Orders/CustomerCreateCustomOrder';
import CustomerCompleteOrder from 'Pages/Public/Orders/CustomerCompleteOrder';
import CustomerOrderInfo from 'Pages/Public/Orders/CustomerOrderInfo';

//- Páginas administrativas
import Login from 'Pages/Admin/Login';
import AdminMain from 'Pages/Admin/Main';
import ServicesGallery from 'Pages/Admin/ServicesGallery';
import ConfigurationsMain from 'Pages/Admin/Configurations/Manage';

//-- Orçamentos
import OrdersMain from 'Pages/Admin/Orders/Main';
import OrdersDetails from 'Pages/Admin/Orders/Details';

//-- Tipos de evento
import EventTypesMain from 'Pages/Admin/EventTypes/Main';
import EventTypesAdd from 'Pages/Admin/EventTypes/Add';
import EventTypesEdit from 'Pages/Admin/EventTypes/Edit';

//-- Fornecedores
import ServiceProvidersMain from 'Pages/Admin/ServiceProviders/Main';
import ServiceProvidersAdd from 'Pages/Admin/ServiceProviders/Add';
import ServiceProvidersEdit from 'Pages/Admin/ServiceProviders/Edit';

//-- Categorias de serviço
import ServiceCategoriesMain from 'Pages/Admin/ServiceCategories/Main';
import ServiceCategoriesAdd from 'Pages/Admin/ServiceCategories/Add';
import ServiceCategoriesEdit from 'Pages/Admin/ServiceCategories/Edit';

//-- Depoimentos
import TestimonialsMain from 'Pages/Admin/Testimonials/Main';
import TestimonialsAdd from 'Pages/Admin/Testimonials/Add';
import TestimonialsEdit from 'Pages/Admin/Testimonials/Edit';

//-- Serviços
import ServicesMain from 'Pages/Admin/Services/Main';
import ServicesAdd from 'Pages/Admin/Services/Add';
import ServicesEdit from 'Pages/Admin/Services/Edit';

//-- Serviços
import DefaultPlansMain from 'Pages/Admin/DefaultPlans/Main';
import DefaultPlansAdd from 'Pages/Admin/DefaultPlans/Add';
import DefaultPlansEdit from 'Pages/Admin/DefaultPlans/Edit';

// - Páginas gerais
import NotFound from 'Pages/Errors/NotFound';


export const paths = [
    {
        'title': 'Página principal',
        'path': '/',
        'exact': true,
        'isPrivate': false,
        'component': PublicMain,
    },
    {
        'title': 'Autenticação',
        'path': '/login',
        'exact': true,
        'isPrivate': false,
        'component': Login,
    },
    {
        'title': 'Novo evento',
        'path': '/novo-evento',
        'exact': true,
        'isPrivate': false,
        'component': BasicCustomerForm,
    },
    {
        'title': 'Planos padrão',
        'path': '/planos',
        'exact': true,
        'isPrivate': false,
        'component': PlanBudgetSelection,
    },
    {
        'title': 'Plano personalizado',
        'path': '/personalizar-evento',
        'exact': true,
        'isPrivate': false,
        'component': CustomerCreateCustomOrder,
    },
    {
        'title': 'Finalizar pedido',
        'path': '/finalizar-pedido',
        'exact': true,
        'isPrivate': false,
        'component': CustomerCompleteOrder,
    },
    {
        'title': 'Orçamento',
        'path': '/orcamento/:orderHash',
        'exact': true,
        'isPrivate': false,
        'component': CustomerOrderInfo,
    },
    {
        'title': 'Painel',
        'path': '/painel',
        'exact': true,
        'isPrivate': true,
        'component': AdminMain,
        'icon': <HomeOutlined />,
    },
    {
        'title': 'Configurações do site',
        'path': '/painel/configuracoes',
        'exact': true,
        'isPrivate': true,
        'component': ConfigurationsMain,
    },
    {
        'title': 'Orçamentos',
        'path': '/painel/orcamentos',
        'exact': true,
        'isPrivate': true,
        'component': OrdersMain,
    },
    {
        'title': 'Detalhes do orçamento',
        'path': '/painel/orcamentos/detalhes/:id',
        'exact': true,
        'isPrivate': true,
        'component': OrdersDetails,
    },
    {
        'title': 'Tipos de evento',
        'path': '/painel/tipos-de-evento',
        'exact': true,
        'isPrivate': true,
        'component': EventTypesMain,
    },
    {
        'title': 'Novo tipo de evento',
        'path': '/painel/tipos-de-evento/adicionar',
        'exact': true,
        'isPrivate': true,
        'component': EventTypesAdd,
    },
    {
        'title': 'Editar tipo de evento',
        'path': '/painel/tipos-de-evento/editar/:id',
        'exact': true,
        'isPrivate': true,
        'component': EventTypesEdit,
    },
    {
        'title': 'Fornecedores',
        'path': '/painel/fornecedores',
        'exact': true,
        'isPrivate': true,
        'component': ServiceProvidersMain,
    },
    {
        'title': 'Novo fornecedor',
        'path': '/painel/fornecedores/adicionar',
        'exact': true,
        'isPrivate': true,
        'component': ServiceProvidersAdd,
    },
    {
        'title': 'Editar fornecedor',
        'path': '/painel/fornecedores/editar/:id',
        'exact': true,
        'isPrivate': true,
        'component': ServiceProvidersEdit,
    },
    {
        'title': 'Categorias de serviço',
        'path': '/painel/categorias',
        'exact': true,
        'isPrivate': true,
        'component': ServiceCategoriesMain,
    },
    {
        'title': 'Nova categoria',
        'path': '/painel/categorias/adicionar',
        'exact': true,
        'isPrivate': true,
        'component': ServiceCategoriesAdd,
    },
    {
        'title': 'Editar categoria',
        'path': '/painel/categorias/editar/:id',
        'exact': true,
        'isPrivate': true,
        'component': ServiceCategoriesEdit,
    },
    {
        'title': 'Depoimentos',
        'path': '/painel/depoimentos',
        'exact': true,
        'isPrivate': true,
        'component': TestimonialsMain,
    },
    {
        'title': 'Novo depoimento',
        'path': '/painel/depoimentos/adicionar',
        'exact': true,
        'isPrivate': true,
        'component': TestimonialsAdd,
    },
    {
        'title': 'Editar depoimento',
        'path': '/painel/depoimentos/editar/:id',
        'exact': true,
        'isPrivate': true,
        'component': TestimonialsEdit,
    },
    {
        'title': 'Serviços',
        'path': '/painel/servicos',
        'exact': true,
        'isPrivate': true,
        'component': ServicesMain,
    },
    {
        'title': 'Novo serviço',
        'path': '/painel/servicos/adicionar',
        'exact': true,
        'isPrivate': true,
        'component': ServicesAdd,
    },
    {
        'title': 'Editar serviço',
        'path': '/painel/servicos/editar/:id',
        'exact': true,
        'isPrivate': true,
        'component': ServicesEdit,
    },
    {
        'title': 'Planos padrão',
        'path': '/painel/planos',
        'exact': true,
        'isPrivate': true,
        'component': DefaultPlansMain,
    },
    {
        'title': 'Novo plano',
        'path': '/painel/planos/adicionar',
        'exact': true,
        'isPrivate': true,
        'component': DefaultPlansAdd,
    },
    {
        'title': 'Editar Plano',
        'path': '/painel/planos/editar/:id',
        'exact': true,
        'isPrivate': true,
        'component': DefaultPlansEdit,
    },
    {
        'title': 'Galerias',
        'path': '/painel/galeria/:id',
        'exact': true,
        'isPrivate': true,
        'component': ServicesGallery,
    },
    {
        'title': 'Página não encontrada',
        'path': '*',
        'exact': false,
        'isPrivate': false,
        'component': NotFound,
    },
];

export default paths;