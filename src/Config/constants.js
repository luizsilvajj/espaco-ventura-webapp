export const HOME_INDICATOR_TOTAL_EVENTS = 'home_total_events';
export const HOME_INDICATOR_FACEBOOK_RATING = 'home_facebook_rating';
export const HOME_INDICATOR_GOOGLE_COMMENTS_RATING = 'home_google_comments_rating';
export const CONFIG_COMMERCIAL_NUMBER = 'order_info_commercial_number';