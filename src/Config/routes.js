import React from "react";
import { HashRouter, Route, Switch, Redirect } from "react-router-dom";

import { isAuthenticated } from "Utils/authentication";
import ScrollToTop from "Components/ScrollToTop";

import paths from "Config/paths";

const PrivateRoute = ({ component: Component, ...rest }) => (

    <Route
        {...rest}
        render={props =>
            isAuthenticated() ? (
                <Component {...props} />
            ) : (
                <Redirect to={{ pathname: "/login", state: { from: props.location } }} />
            )
        }
    />
);

const Routes = () => (
    <HashRouter>
        <ScrollToTop />
        <Switch>
            {paths.map((item) => {
                return item.isPrivate ? <PrivateRoute key={item.path} {...item} /> : <Route key={item.path} {...item} />;
            })}
        </Switch>
    </HashRouter>
);

export default Routes;