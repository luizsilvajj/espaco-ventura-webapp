import { configureStore } from '@reduxjs/toolkit';
import rootReducer from 'Redux/modules/rootReducer';

const store = configureStore({ reducer: rootReducer });

export default store;