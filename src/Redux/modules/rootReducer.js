import { combineReducers } from 'redux';

import systemAlert from './systemAlert/reducer';

export default combineReducers({
    systemAlert
});