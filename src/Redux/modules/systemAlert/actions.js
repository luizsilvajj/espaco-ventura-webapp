import {createAction} from '@reduxjs/toolkit';

export const add = createAction('systemAlert/add');
export const clear = createAction('systemAlert/clear');