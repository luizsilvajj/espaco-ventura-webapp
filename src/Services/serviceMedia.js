import { get, post, put, destroy } from "Utils/api";

//Define a rota base desse serviço
const baseRoute = "service_media";


export const findById = async (id) => {
    return await get(baseRoute, "/" + id);
};

export const add = async (postData) => {
    var response = null;

    if (postData.file) {
        const file = postData.file;

        var formData = new FormData();
        var headers = { 'Content-Type': 'multipart/form-data' };

        formData.append('service_id', postData.service_id);
        formData.append('content_type', "image");
        formData.append('content', file.originFileObj, file.filename);

        response = await post(baseRoute, "", formData, headers);
    }

    return response;
};

export const update = async (id, postData) => {
    var data = JSON.stringify(postData);
    return await put(baseRoute, "", id, data);
};

export const remove = async (id) => {
    return await destroy(baseRoute, "", id);
};

const servicesService = {
    findById,
    add,
    update,
    remove,
}

export default servicesService;