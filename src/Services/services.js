import { get, post, put, destroy } from "Utils/api";

//Define a rota base desse serviço
const baseRoute = "services";


export const findAll = async () => {
    return await get(baseRoute);
};

export const findPlans = async () => {
    return await get(baseRoute, "/plans");
};

export const findCustom = async () => {
    return await get(baseRoute, "/custom");
};

export const findPublicPlans = async () => {
    return await get(baseRoute, "/public/plans");
};

export const findPublicCustom = async () => {
    return await get(baseRoute, "/public/custom");
};

export const findById = async (id) => {
    return await get(baseRoute, "/" + id);
};

export const findPublicById = async (id) => {
    return await get(baseRoute, "/public/" + id);
};

export const findMediaById = async (id) => {
    return await get(baseRoute, "/" + id + "/media");
};

export const add = async (postData) => {
    var data = JSON.stringify(postData);
    return await post(baseRoute, "", data);
};

export const update = async (id, postData) => {
    var data = JSON.stringify(postData);
    return await put(baseRoute, "", id, data);
};

export const remove = async (id) => {
    return await destroy(baseRoute, "", id);
};

const servicesService = {
    findAll,
    findPlans,
    findCustom,
    findPublicPlans,
    findPublicCustom,
    findById,
    findPublicById,
    findMediaById,
    add,
    update,
    remove,
}

export default servicesService;