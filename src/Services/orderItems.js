import { get, post, put, destroy } from "Utils/api";

//Define a rota base desse serviço
const baseRoute = "order_items";


export const findAll = async () => {
    return await get(baseRoute);
};

export const findById = async (id) => {
    return await get(baseRoute, "/" + id);
};

export const add = async (postData) => {
    var data = JSON.stringify(postData);
    return await post(baseRoute, "", data);
};

export const update = async (id, postData) => {
    var data = JSON.stringify(postData);
    return await put(baseRoute, "", id, data);
};

export const remove = async (id) => {
    return await destroy(baseRoute, "", id);
};

export const deleteItem = async (orderId, serviceId) => {
    return await get(baseRoute, "/delete-item/" + orderId + "/" + serviceId);
};

const orderItemsService = {
    findAll,
    findById,
    add,
    update,
    remove,
    deleteItem,
}

export default orderItemsService;