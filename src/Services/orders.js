import { get, post, put, destroy } from "Utils/api";

//Define a rota base desse serviço
const baseRoute = "orders";


export const findAll = async () => {
    return await get(baseRoute);
};

export const findById = async (id) => {
    return await get(baseRoute, "/find", { order_id: id });
};

export const findByHash = async (orderHash) => {
    return await get(baseRoute, "/find", { order_hash: orderHash });
};

export const publicFindByHash = async (orderHash) => {
    return await get(baseRoute, "/public/find", { order_hash: orderHash });
};

export const createHash = async () => {
    return await post(baseRoute, "/new-order", []);
};

export const update = async (id, postData) => {
    var data = JSON.stringify(postData);
    return await put(baseRoute, "", id, data);
};

export const publicUpdate = async (id, postData) => {
    var data = JSON.stringify(postData);
    return await put(baseRoute, "/update-order", id, data);
};

export const remove = async (id) => {
    return await destroy(baseRoute, "", id);
};

export const deleteItems = async (orderId) => {
    return await get(baseRoute, "/delete-items/" + orderId);
};

const ordersService = {
    findAll,
    findById,
    findByHash,
    publicFindByHash,
    publicUpdate,
    createHash,
    update,
    deleteItems,
    remove,
}

export default ordersService;