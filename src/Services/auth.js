import { get, post, put } from "Utils/api";

//Define a rota base desse serviço
const baseRoute = "auth";

export const login = async (email, password) => {
    var data = JSON.stringify({ email: email, password: password });
    return await post(baseRoute, "/login", data);
};


export const logout = async () => {
    return await post(baseRoute, "/logout");
};

export const update = async (postData) => {
    var data = JSON.stringify(postData);
    return await post(baseRoute, "/update", data);
};

const authService = {
    login,
    logout,
    update,
}

export default authService;