import { get, post, put, destroy } from "Utils/api";

//Define a rota base desse serviço
const baseRoute = "testimonials";


export const findAll = async () => {
    return await get(baseRoute);
};

export const findById = async (id) => {
    return await get(baseRoute, "/" + id);
};

export const add = async (postData) => {
    var response = null;

    if (postData.fileList.length > 0) {
        const file = postData.fileList[0];

        var formData = new FormData();
        var headers = { 'Content-Type': 'multipart/form-data' };

        formData.append('customer_name', postData.customer_name);
        formData.append('customer_picture', file.originFileObj, file.filename);
        formData.append('content', postData.content);

        if (typeof (postData.testimonial_date) !== 'undefined' && postData.testimonial_date != '') {
            formData.append('testimonial_date', postData.testimonial_date);
        }

        response = await post(baseRoute, "", formData, headers);

    } else {
        if (typeof (postData.testimonial_date) === 'undefined' || postData.testimonial_date === '') {
            delete postData.testimonial_date;
        }
        var data = JSON.stringify(postData);
        response = await post(baseRoute, "", data);
    }

    return response;
};

export const update = async (id, postData) => {
    var response = null;

    if (postData.fileList.length > 0) {
        const file = postData.fileList[0];

        var formData = new FormData();
        var headers = { 'Content-Type': 'multipart/form-data' };

        formData.append("_method", 'PUT');

        //Só altera o arquivo se não for a foto que veio da API
        if (file.uid !== "-1") {
            formData.append('customer_picture', file.originFileObj, file.filename);
        }

        formData.append('customer_name', postData.customer_name);
        formData.append('content', postData.content);
        formData.append('remove_avatar', postData.remove_avatar);

        if (typeof (postData.testimonial_date) !== 'undefined' && postData.testimonial_date !== '' && postData.testimonial_date) {
            formData.append('testimonial_date', postData.testimonial_date);
        }

        response = await post(baseRoute, "/" + id, formData, headers);

    } else {
        postData._method = "PUT";

        if (typeof (postData.testimonial_date) === 'undefined' || postData.testimonial_date === '') {
            delete postData.testimonial_date;
        }

        var data = JSON.stringify(postData);
        response = await post(baseRoute, "/" + id, data);
    }

    return response;
};

export const remove = async (id) => {
    return await destroy(baseRoute, "", id);
};

const testimonialsService = {
    findAll,
    findById,
    add,
    update,
    remove,
}

export default testimonialsService;