import { get, post, put, destroy } from "Utils/api";

//Define a rota base desse serviço
const baseRoute = "service_categories";


export const findAll = async () => {
    return await get(baseRoute);
};

export const findDefault = async () => {
    return await get(baseRoute, "/default");
};

export const findById = async (id) => {
    return await get(baseRoute, "/" + id);
};

export const add = async (postData) => {
    var data = JSON.stringify(postData);
    return await post(baseRoute, "", data);
};

export const update = async (id, postData) => {
    var data = JSON.stringify(postData);
    return await put(baseRoute, "", id, data);
};

export const remove = async (id) => {
    return await destroy(baseRoute, "", id);
};

export const reorder = async (postData) => {
    var data = JSON.stringify(postData);
    return await post(baseRoute, "reorder", data);
};

const serviceCategoriesService = {
    findAll,
    findDefault,
    findById,
    add,
    update,
    remove,
    reorder,
}

export default serviceCategoriesService;