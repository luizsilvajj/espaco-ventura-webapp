import { get, post, put, destroy } from "Utils/api";

//Define a rota base desse serviço
const baseRoute = "configurations";


export const findAll = async () => {
    return await get(baseRoute);
};

export const findBySlug = async (slug) => {
    return await get(baseRoute, "/" + slug);
};

export const add = async (postData) => {
    var data = JSON.stringify(postData);
    return await post(baseRoute, "", data);
};

export const update = async (slug, postData) => {
    var data = JSON.stringify(postData);
    return await put(baseRoute, "", slug, data);
};

export const remove = async (slug) => {
    return await destroy(baseRoute, "", slug);
};

const configurationsService = {
    findAll,
    findBySlug,
    add,
    update,
    remove,
}

export default configurationsService;