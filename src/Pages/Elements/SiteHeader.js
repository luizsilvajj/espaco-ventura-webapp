import React from 'react';
import { Layout, Menu, Drawer } from 'antd';
import { MenuOutlined } from '@ant-design/icons';
import 'antd/dist/antd.css';
import 'assets/styles/app.css';
import styles from 'assets/styles/styles';

import authService from "Services/auth";
import { logout, getUser } from "Utils/authentication";

class SiteHeader extends React.Component {

    state = {
        visible: false
    };

    goTo = (destination) => {
        const { history } = this.props;
        //redirect
        history.push(destination);
    };

    goToBrandSite = () => {
        window.open("https://espacoventura.com.br", "_blank");
    };


    toggleDrawer = () => {
        this.setState({
            visible: !this.state.visible,
        });
    };

    logoutFunction = async () => {
        const response = await authService.logout();
        logout();
        this.goTo("/login");
    };


    render() {
        const { Header } = Layout;
        const currentUser = getUser();
        let { light } = this.props;


        return (
            <Header className={light ? "site-header light" : "site-header"}>
                <a onClick={() => this.goTo('/')}><div className="site-logo" /></a>
                <a onClick={(e) => { this.toggleDrawer(); }} className="site-menu-trigger"> <MenuOutlined /></a>
                <Drawer
                    placement="right"
                    closable={false}
                    onClose={this.toggleDrawer}
                    visible={this.state.visible}
                >
                    <Menu
                        mode={"vertical"}
                    >
                        <Menu.Item onClick={() => this.goTo('/painel')}> Painel administrativo</Menu.Item>
                        {currentUser !== null ? <Menu.Item onClick={() => this.logoutFunction()}>Sair</Menu.Item> : null}
                    </Menu>
                </Drawer>
                <Menu className={light ? "site-menu light" : "site-menu"} mode="horizontal" >
                    <Menu.Item key="1" className="brand-menu" onClick={this.goToBrandSite}>Espaço Ventura</Menu.Item>
                </Menu>
            </Header>
        );
    }
}

export default SiteHeader;
