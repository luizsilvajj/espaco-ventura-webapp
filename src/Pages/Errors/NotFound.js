import React from 'react';
import { Layout, Button, Row, Col } from 'antd';
import SiteHeader from 'Pages/Elements/SiteHeader';
import 'antd/dist/antd.css';
import 'assets/styles/app.css';

class NotFound extends React.Component {

  goToPreviousPage = () => {
    const { history } = this.props;
    //redirect
    history.goBack();
  };

  render() {
    const { history } = this.props;
    const { Content } = Layout;

    return (
      <Layout className="main-content">
        <SiteHeader history={history} />
        <Content className="site-layout">
          <Row>
            <Col className="not-found-intro" span={24}>
              <h2 className="highlight">Erro 404</h2>
              <p>Página não encontrada...</p>
              <Button onClick={this.goToPreviousPage} className={"action-button"} size={'large'} type={"primary"}> Voltar para página anterior </Button>
            </Col>
          </Row>
        </Content>
      </Layout >
    );
  }
}

export default NotFound;