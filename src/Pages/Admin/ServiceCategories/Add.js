import React from 'react';
import { connect } from "react-redux";
import { add as addSystemAlert } from "Redux/modules/systemAlert/actions";

import { Layout, Row, Col, Button, Form, Input, Space, Alert, Divider } from 'antd';
import 'antd/dist/antd.css';
import 'assets/styles/app.css';

import SiteHeader from 'Pages/Elements/SiteHeader';
import SiteBreadcrumb from 'Pages/Elements/SiteBreadcrumb';
import PageLoadingIndicator from 'Components/PageLoadingIndicator';


import serviceCategoriesService from 'Services/serviceCategories';

class Add extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      form: {},
      actionLoading: false,
      error: false,
      message: '',
      data: [],
    };
  }


  goTo = (destination) => {
    const { history } = this.props;
    //redirect
    history.push(destination);
  };

  goToPreviousPage = () => {
    const { history } = this.props;
    //redirect
    history.goBack();
  };


  handleSubmit = async (values) => {
    this.setState({ loading: true, actionLoading: true, error: '', form: { ...values } });
    const { title } = values;
    if (!title) {
      this.setState({ loading: false, error: "Preencha um título para a categoria de serviço para continuar..." });
    } else {
      let postData = values;
      try {
        const response = await serviceCategoriesService.add(postData);
        if (response.error == true) {
          this.setState({
            loading: false,
            actionLoading: false,
            error: true,
            message: response.message
          });
        } else {
          await this.props.addSystemAlert({ message: "Categoria de serviço adicionada com sucesso", type: "success" });
          this.goTo("/painel/categorias");
        }
      } catch (err) {
        this.setState({
          loading: false,
          actionLoading: false,
          error: true,
          message: err.message
        });
      }
    }
  };


  render() {
    const { history, location } = this.props;
    const { Content } = Layout;
    const { loading, actionLoading, error, message } = this.state;


    return (
      <Layout className="main-content">
        <SiteHeader history={history} />
        <Content className="site-layout">
          <div className="site-content">
            <SiteBreadcrumb location={location} />
            {loading === true && <PageLoadingIndicator loadingText={actionLoading ? "Por favor, aguarde..." : "Carregando informações..."} />}
            {loading === false && error === true && <Alert className="m-b-20" message={"Erro"} description={message} type="error" showIcon />}
            {loading === false && (
              <Row>
                <Col span={24}>
                  <Divider orientation="left" plain>
                    Dados básicos
                  </Divider>
                </Col>
                <Col span={24}>
                  <Form
                    layout="vertical"
                    name="serviceCategory"
                    initialValues={{ ...this.state.form }}
                    onFinish={this.handleSubmit}
                  >
                    <Row>
                      <Col span={24}>
                        <Form.Item
                          label="Título"
                          name="title"
                          rules={[{ required: true, message: 'Informe um título para a categoria de serviço...' }]}
                        >
                          <Input type={"text"} className="default-input" />
                        </Form.Item>

                      </Col>
                    </Row>
                    <Divider className="form-actions-separator" />
                    <Form.Item style={{ float: 'right' }}>
                      <Space size={30}>
                        <Button onClick={() => this.goToPreviousPage()} className="action-button-cancel">
                          Mudei de ideia
                        </Button>
                        <Button disabled={loading} className={"action-button"} type="primary" htmlType="submit">
                          Salvar
                        </Button>
                      </Space>
                    </Form.Item>
                  </Form>
                </Col>
              </Row>
            )}
          </div>
        </Content>
      </Layout >
    );
  }
}


//Configs Redux para este component
//
//Define quais atributos vou pegar do
//state do Redux
const mapStateToProps = null;

//Define quais ações esse component
//vai usar para interagir com o Redux
const mapDispatchToProps = (dispatch) => {
  return {
    addSystemAlert: (actionFeedbackMessage) => dispatch(addSystemAlert(actionFeedbackMessage))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Add);