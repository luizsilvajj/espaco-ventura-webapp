import React from 'react';
import { connect } from "react-redux";
import arrayMove from 'array-move';
import { clear as clearSytemAlert } from "Redux/modules/systemAlert/actions";

import { Layout, Space, Alert, Button, Table, Tooltip, Popconfirm } from 'antd';
import 'antd/dist/antd.css';
import 'assets/styles/app.css';

import SiteHeader from 'Pages/Elements/SiteHeader';
import SiteBreadcrumb from 'Pages/Elements/SiteBreadcrumb';

import PageLoadingIndicator from 'Components/PageLoadingIndicator';
import { DragHandle, SortableItem, SortableContainer } from 'Components/SortableRowHandler';

import { PlusOutlined, EditOutlined, DeleteOutlined, ExclamationCircleOutlined, InboxOutlined } from '@ant-design/icons';

import { handleResultData, flash } from 'Utils/functions';
import serviceCategoriesService from 'Services/serviceCategories';


class Main extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      actionLoading: false,
      error: false,
      message: '',
      data: [],
    };
    this.loadData = this.loadData.bind(this);
  }

  async componentDidMount() {
    const { actionFeedbackMessage } = this.props;

    if (actionFeedbackMessage) {
      flash(actionFeedbackMessage);
      this.props.clearSytemAlert();
    }

    await this.loadData();
  }

  loadData = async () => {
    await serviceCategoriesService.findAll().then((result) => {
      let handledResult = handleResultData(result);
      this.setState({ ...handledResult, loading: false });
    });
  }

  goTo = (destination) => {
    const { history } = this.props;
    //redirect
    history.push(destination);
  };

  deleteItem = async (recordItem) => {
    const { loadData } = this;
    if (recordItem.id) {
      this.setState({ loading: true, actionLoading: true });
      await serviceCategoriesService.remove(recordItem.id).then((result) => {
        let { error, message } = result;
        this.setState({ loading: false, actionLoading: false }, () => {
          loadData();
          if (error == false) {
            flash({ message: 'Item removido com sucesso!', type: 'success' });
          } else {
            flash({ message: 'Erro ao tentar realizar a ação: ' + message, type: 'error' });
          }
        });
      });
    }
  }

  onSortEnd = async ({ oldIndex, newIndex }) => {
    const { data } = this.state;
    if (oldIndex !== newIndex) {
      const newData = arrayMove([].concat(data), oldIndex, newIndex).filter(el => !!el);
      let postData = {}
      postData.order_data = newData.map((item, dataIndex) => {
        return {
          'service_category_id': item.id,
          'weight': dataIndex,
        }
      });
      var response = await serviceCategoriesService.reorder(postData);
      if (response.error == true) {
        flash({ message: response.message, type: 'error' });
      } else {
        this.setState({ data: newData }, () => {
          flash({ message: 'As alterações foram salvas com sucesso...', type: 'success' });
        });
      }
    }
  };

  DraggableContainer = props => (
    <SortableContainer
      useDragHandle
      disableAutoscroll
      helperClass="row-dragging"
      onSortEnd={this.onSortEnd}
      {...props}
    />
  );

  DraggableBodyRow = ({ className, style, ...restProps }) => {
    const { data } = this.state;
    // function findIndex base on Table rowKey props and should always be a right array index
    const index = data.findIndex(x => x.index === restProps['data-row-key']);
    return <SortableItem index={index} {...restProps} />;
  };

  render() {
    const { history, location } = this.props;
    const { Content } = Layout;
    const { loading, actionLoading, error, message, data } = this.state;


    const columns = [
      {
        title: 'Ordem',
        dataIndex: 'index',
        width: 30,
        className: 'drag-visible',
        render: () => <DragHandle />,
      },
      {
        title: "Categoria de serviço",
        dataIndex: 'title',
      },
      {
        title: "Status",
        dataIndex: 'is_active',
        width: '10%',
        render: status => (status == true ? "Ativo" : "Inativo")
      },
      {
        title: '',
        key: 'action',
        width: '20%',
        align: 'right',
        render: (text, record) => (
          <Space size="middle">
            <Tooltip title="Editar">
              <Button onClick={() => this.goTo("/painel/categorias/editar/" + record.id)} className="table-action" shape="round" icon={<EditOutlined />} />
            </Tooltip>
            <Tooltip title="Excluir">
              <Popconfirm
                icon={<ExclamationCircleOutlined className="danger-action" />}
                placement="leftBottom" title={<span>Tem certeza<br /> Essa ação não pode ser revertida!</span>}
                okText="Sim"
                cancelText="Mudei de ideia"
                okButtonProps={{ className: "action-button" }}
                cancelButtonProps={{ className: "secondary-action" }}
                onConfirm={(e) => this.deleteItem(record)}
              >
                <Button className="table-action" shape="round" icon={<DeleteOutlined />} />
              </Popconfirm>
            </Tooltip>
          </Space>
        ),
      },
    ];


    return (
      <Layout className="main-content">
        <SiteHeader history={history} />
        <Content className="site-layout">
          <div className="site-content">
            <SiteBreadcrumb location={location} />
            {loading === true && <PageLoadingIndicator loadingText={actionLoading ? "Por favor, aguarde..." : "Carregando informações..."} />}
            {loading === false && error === true && <Alert className="m-b-20" message={"Erro"} description={message} type="error" showIcon />}
            {loading === false && error === false && data.length <= 0 && <Alert className="m-b-20" message={"Sem informações"} description={"Não há dados para exibir no momento..."} type="warning" showIcon closable />}
            {loading === false && (
              <div>
                <Button onClick={() => this.goTo("/painel/categorias/adicionar")} className="action-button pull-right" type={"primary"} icon={<PlusOutlined />}>
                  Adicionar
                </Button>
                <Table
                  pagination={false}
                  locale={{
                    emptyText: (<span><InboxOutlined className="f-s-40" /> <br />Nenhum dado para exibir no momento...</span>)
                  }}
                  columns={columns}
                  dataSource={data}
                  rowKey="index"
                  components={{
                    body: {
                      wrapper: this.DraggableContainer,
                      row: this.DraggableBodyRow,
                    }
                  }}
                />
              </div>
            )}
          </div>
        </Content>
      </Layout >
    );
  }
}


//Configs Redux para este component
//
//Define quais atributos vou pegar do
//state do Redux
const mapStateToProps = (state) => {
  const { actionFeedbackMessage } = state.systemAlert;
  return { actionFeedbackMessage: actionFeedbackMessage };
};

//Define quais ações esse component
//vai usar para interagir com o Redux
const mapDispatchToProps = (dispatch) => {
  return {
    clearSytemAlert: () => dispatch(clearSytemAlert())
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);