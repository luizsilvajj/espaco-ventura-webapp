import React from 'react';
import { connect } from "react-redux";
import { add as addSystemAlert } from "Redux/modules/systemAlert/actions";

import { Layout, Row, Col, Button, Form, Space, Alert, Divider, Upload } from 'antd';
import ImgCrop from 'antd-img-crop';
import 'antd/dist/antd.css';
import 'assets/styles/app.css';

import SiteHeader from 'Pages/Elements/SiteHeader';
import SiteBreadcrumb from 'Pages/Elements/SiteBreadcrumb';
import PageLoadingIndicator from 'Components/PageLoadingIndicator';

import servicesService from 'Services/services';
import serviceMediaService from 'Services/serviceMedia';

class Gallery extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      actionLoading: false,
      error: false,
      message: '',
      showForm: true,
      allowSubmit: true,
      serviceName: "",
      fileList: [],
      filesToDelete: [],
      locale: 'pt-br'
    };
  }

  goTo = (destination) => {
    const { history } = this.props;
    //redirect
    history.push(destination);
  };

  goToPreviousPage = () => {
    const { history } = this.props;
    //redirect
    history.goBack();
  };

  async componentDidMount() {
    const { id } = this.props.match.params;
    await this.loadData(id);

  }

  loadData = async (id) => {

    const [servicesResponse, serviceMediaResponse] = await Promise.all([
      servicesService.findById(id),
      servicesService.findMediaById(id)
    ]);

    let error = false, message = "", fileList = [], serviceName = "";

    if (servicesResponse.error === false) {
      serviceName = servicesResponse.data.title;
      let { error: mediaError, message: mediaMessage, data: mediaData } = serviceMediaResponse;
      if (mediaError === false) {
        error = false;

        if (mediaData && mediaData.length > 0) {
          mediaData.map((item, index) => {
            fileList.push({
              uid: item.id,
              service_media_id: item.id,
              name: item.content,
              status: 'done',
              url: item.content_url
            });
          });
        }
      } else {
        error = true;
        message = mediaMessage;
      }
    } else {
      error = true;
      message = servicesResponse.message;
    }


    this.setState({ error: error, message: message, serviceName: serviceName, fileList: fileList, loading: false });


  }

  normFile = (e) => {
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  localUpload = ({ file, onSuccess }) => {
    setTimeout(() => {
      onSuccess("ok");
    }, 0);
  };

  onChange = (newFileList) => {
    const fileListToSave = newFileList.fileList || [];
    const removeAvatar = fileListToSave.length > 0 ? false : true;
    this.setState({ fileList: fileListToSave, removeAvatar });
  };

  onRemove = (file) => {
    const filesToDelete = this.state.filesToDelete || [];
    if (file.service_media_id) {
      filesToDelete.push(file.service_media_id);
    }
    this.setState({ filesToDelete: filesToDelete });
  };

  onPreview = async file => {
    let src = file.url;
    if (!src) {
      src = await new Promise(resolve => {
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj);
        reader.onload = () => resolve(reader.result);
      });
    }
    const image = new Image();
    image.src = src;
    const imgWindow = window.open(src);
    imgWindow.document.write(image.outerHTML);
  };

  handleSubmit = async (values) => {
    this.setState({ loading: true, actionLoading: true, error: false });
    const { id } = this.props.match.params;
    const { fileList, filesToDelete } = this.state;
    let toSave = [], saveResults = null, deleteResults = null, error = false, message = "";


    if (fileList.length > 0) {
      fileList.map((fileItem, index) => {
        if (!fileItem.service_media_id) {
          toSave.push({
            service_id: id,
            file: fileItem,
          });
        }
      });
    }

    if (toSave.length > 0) {
      saveResults = await Promise.all(toSave.map(async (item) => {
        return await serviceMediaService.add(item);
      }));
    }

    if (filesToDelete.length > 0) {
      deleteResults = await Promise.all(filesToDelete.map(async (item) => {
        return await serviceMediaService.remove(item);
      }));
    }

    if (saveResults || deleteResults) {
      const savedResults = saveResults || [];
      const deletedResults = deleteResults || [];

      const hasSaveErrors = savedResults.some((savedItem) => { return savedItem.error === true });
      const hasDeleteErrors = deletedResults.some((savedItem) => { return savedItem.error === true });
      let errorMessages = [];
      if (hasDeleteErrors || hasSaveErrors) {
        //const message = hasSaveErrors ? (hasDeleteErrors ? "Houve um ou mais erros ao tentar incluir as novas fotos e excluir as antigas... Por favor, tente novamente mais tarde...": "Houve um ou mais erros ao tentar incluir novas fotos... Por favor, tente novamente mais tarde..." ) : "Hove um ou mais erros ao tentar remover as fotos antigas... Por favor, tente novamente mais tarde...";
        for (var i = 0; i < savedResults.length; i++) {
          if (savedResults[i].error === true) {
            errorMessages.push(savedResults[i].message);
          }
        }
        for (var j = 0; j < deletedResults.length; j++) {
          if (deletedResults[j].error === true) {
            errorMessages.push(deletedResults[j].message);
          }
        }
        error = true;
        message = errorMessages.join(" | ");
        this.setState({
          loading: false,
          actionLoading: false,
          error: error,
          message: message
        });
      } else {
        await this.props.addSystemAlert({ message: "As alterações foram realizadas com sucesso!", type: "success" });
        this.goToPreviousPage();
      }
    } else {
      await this.props.addSystemAlert({ message: "Não foram realizadas alterações...", type: "info" });
      this.goToPreviousPage();
    }


  };

  render() {
    const { history, location } = this.props;
    const { Content } = Layout;
    const { loading, actionLoading, error, message, allowSubmit, fileList, serviceName } = this.state;
    const { normFile, onChange, onPreview, onRemove, localUpload } = this;

    return (
      <Layout className="main-content">
        <SiteHeader history={history} />
        <Content className="site-layout">
          <div className="site-content">
            <SiteBreadcrumb location={location} />
            {loading === true && <PageLoadingIndicator loadingText={actionLoading ? "Por favor, aguarde..." : "Carregando informações..."} />}
            {loading === false && error === true && <Alert className="m-b-20" message={"Erro"} description={message} type="error" showIcon />}
            {loading === false && (
              <Row>
                <Col span={24}>
                  <Divider orientation="left" plain>
                    Galeria
                  </Divider>
                </Col>
                <Col span={24}>
                  <p>Escolha as fotos que ficarão disponíveis na descrição do serviço <em>{serviceName}</em></p>
                  <Form
                    layout="vertical"
                    name="serviceProviderGallery"
                    initialValues={{ ...this.state.form }}
                    onFinish={this.handleSubmit}
                  >
                    <Row>
                      <Col span={24}>
                        <Form.Item
                          label={false}
                          name="content"
                          valuePropName="media_content"
                          getValueFromEvent={normFile}
                        >
                          <ImgCrop modalTitle={"Editar imagem"} modalCancel={"Mudei de ideia"} modalOk={"Enviar"} rotate>
                            <Upload
                              listType="picture-card"
                              fileList={fileList}
                              name={'media_content'}
                              onRemove={onRemove}
                              onChange={onChange}
                              onPreview={onPreview}
                              customRequest={localUpload}
                            >
                              {fileList.length < 10 && '+ Escolher'}
                            </Upload>
                          </ImgCrop>
                        </Form.Item>
                        <p>Você pode inserir até 10 imagens...</p>
                      </Col>
                    </Row>
                    <Divider className="form-actions-separator" />
                    <Form.Item style={{ float: 'right' }}>
                      <Space size={30}>
                        <Button onClick={() => this.goToPreviousPage()} className="action-button-cancel">
                          {allowSubmit === true ? 'Mudei de ideia' : 'Voltar para página anterior'}
                        </Button>
                        {allowSubmit === true && <Button disabled={loading} className={"action-button"} type="primary" htmlType="submit"> Salvar </Button>}
                      </Space>
                    </Form.Item>
                  </Form>
                </Col>
              </Row>
            )}
          </div>
        </Content>
      </Layout >
    );
  }
}


//Configs Redux para este component
//
//Define quais atributos vou pegar do
//state do Redux
const mapStateToProps = null;

//Define quais ações esse component
//vai usar para interagir com o Redux
const mapDispatchToProps = (dispatch) => {
  return {
    addSystemAlert: (actionFeedbackMessage) => dispatch(addSystemAlert(actionFeedbackMessage))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Gallery);