import React from 'react';
import { connect } from "react-redux";
import { clear as clearSytemAlert } from "Redux/modules/systemAlert/actions";

import { Layout, Space, Alert, Button, Table, Tooltip, Badge } from 'antd';
import 'antd/dist/antd.css';
import 'assets/styles/app.css';


import SiteHeader from 'Pages/Elements/SiteHeader';
import SiteBreadcrumb from 'Pages/Elements/SiteBreadcrumb';
import PageLoadingIndicator from 'Components/PageLoadingIndicator';
import { EyeOutlined, DeleteOutlined, ExclamationCircleOutlined, InboxOutlined } from '@ant-design/icons';

import { handleResultData, flash } from 'Utils/functions';
import ordersService from 'Services/orders';
import moment from 'moment';


class Main extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      actionLoading: false,
      error: false,
      message: '',
      data: [],
    };
    this.loadData = this.loadData.bind(this);
  }

  async componentDidMount() {
    const { actionFeedbackMessage } = this.props;

    if (actionFeedbackMessage) {
      flash(actionFeedbackMessage);
      this.props.clearSytemAlert();
    }

    await this.loadData();
  }

  loadData = async () => {
    await ordersService.findAll().then((result) => {
      let handledResult = handleResultData(result);
      this.setState({ ...handledResult, loading: false });
    });
  }

  goTo = (destination) => {
    const { history } = this.props;
    //redirect
    history.push(destination);
  };

  deleteItem = async (recordItem) => {
    const { loadData } = this;
    if (recordItem.id) {
      this.setState({ loading: true, actionLoading: true });
      await ordersService.remove(recordItem.id).then((result) => {
        let { error, message } = result;
        this.setState({ loading: false, actionLoading: false }, () => {
          loadData();
          if (error == false) {
            flash({ message: 'Pedido removido com sucesso!', type: 'success' });
          } else {
            flash({ message: 'Erro ao tentar realizar a ação: ' + message, type: 'error' });
          }
        });
      });
    }
  }

  render() {
    const { history, location } = this.props;
    const { Content } = Layout;
    const { loading, actionLoading, error, message, data } = this.state;


    const columns = [
      {
        title: "#",
        dataIndex: 'id',
        width: '10%',
      },
      {
        title: "Nome",
        dataIndex: 'customer_name',
        width: '30%',
      },
      {
        title: "Telefone",
        dataIndex: 'customer_phone',
        width: '20%',
      },
      {
        title: "Enviado em",
        dataIndex: 'created',
        width: '10%',
        render: created => moment(created).format('DD/MM/YYYY HH:mm')
      },
      {
        title: "Cliente completou?",
        dataIndex: 'customer_completed',
        width: '10%',
        render: customer_completed => (customer_completed == true ? <Badge status="success" text="Sim" /> : <Badge status="processing" text="Não" />)
      },
      {
        title: "Já foi atendido?",
        dataIndex: 'institution_completed',
        width: '10%',
        render: institution_completed => (institution_completed == true ? <Badge status="success" text="Sim" /> : <Badge status="error" text="Não" />)
      },
      {
        title: '',
        key: 'action',
        width: '10%',
        align: 'right',
        render: (text, record) => (
          <Space size="middle">
            <Tooltip title="Ver">
              <Button onClick={() => this.goTo("/painel/orcamentos/detalhes/" + record.id)} className="table-action" shape="round" icon={<EyeOutlined />} />
            </Tooltip>
          </Space>
        ),
      },
    ];


    return (
      <Layout className="main-content">
        <SiteHeader history={history} />
        <Content className="site-layout">
          <div className="site-content">
            <SiteBreadcrumb location={location} />
            {loading === true && <PageLoadingIndicator loadingText={actionLoading ? "Por favor, aguarde..." : "Carregando informações..."} />}
            {loading === false && error === true && <Alert className="m-b-20" message={"Erro"} description={message} type="error" showIcon />}
            {loading === false && error === false && data.length <= 0 && <Alert className="m-b-20" message={"Sem informações"} description={"Não há dados para exibir no momento..."} type="warning" showIcon closable />}
            {loading === false && (
              <div>
                <Table
                  locale={{
                    emptyText: (<span><InboxOutlined className="f-s-40" /> <br />Nenhum dado para exibir no momento...</span>)
                  }}
                  columns={columns}
                  dataSource={data}
                />
              </div>
            )}
          </div>
        </Content>
      </Layout >
    );
  }
}


//Configs Redux para este component
//
//Define quais atributos vou pegar do
//state do Redux
const mapStateToProps = (state) => {
  const { actionFeedbackMessage } = state.systemAlert;
  return { actionFeedbackMessage: actionFeedbackMessage };
};

//Define quais ações esse component
//vai usar para interagir com o Redux
const mapDispatchToProps = (dispatch) => {
  return {
    clearSytemAlert: () => dispatch(clearSytemAlert())
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);