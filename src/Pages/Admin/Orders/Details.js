import React from 'react';
import { connect } from "react-redux";
import { clear as clearSytemAlert } from "Redux/modules/systemAlert/actions";

import { Layout, Space, Alert, Button, Descriptions, Badge, Switch } from 'antd';
import 'antd/dist/antd.css';
import 'assets/styles/app.css';


import SiteHeader from 'Pages/Elements/SiteHeader';
import SiteBreadcrumb from 'Pages/Elements/SiteBreadcrumb';
import PageLoadingIndicator from 'Components/PageLoadingIndicator';

import ordersService from 'Services/orders';
import moment from 'moment';

import { currencyFormatter, handleResultData } from 'Utils/functions';

class Details extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      actionLoading: false,
      error: false,
      message: '',
      data: [],
    };
    this.loadData = this.loadData.bind(this);
  }

  componentDidMount() {
    const { id } = this.props.match.params;
    this.loadData(id);
  }

  loadData = async (id) => {
    await ordersService.findById(id).then((result) => {
      const { error, message, data } = result;
      this.setState({ error: error, message: message, data: data, loading: false });
    });
  }

  goToPreviousPage = () => {
    const { history } = this.props;
    //redirect
    history.goBack();
  };

  changeOrderInstitutionStatus = async (orderId, newStatus) => {
    let oldData = this.state.data;
    let dateTime = moment().format('YYYY-MM-DD HH:mm:ss');
    let params = {
      institution_completed: newStatus,
      institution_completed_date: newStatus == 1 ? dateTime : null
    };

    this.setState({ loading: true });
    await ordersService.update(orderId, params).then((result) => {
      let handledResult = handleResultData(result);
      if (handledResult.error == true) {
        this.setState({ formError: true, formMessage: handledResult.message, loading: false });
      } else {
        //redirect
        oldData.institution_completed = newStatus;
        this.setState({ data: oldData, loading: false })
      }
    });
  };


  render() {
    const { history, location } = this.props;
    const { Content } = Layout;
    const { loading, actionLoading, error, message, data } = this.state;
    return (
      <Layout className="main-content">
        <SiteHeader history={history} />
        <Content className="site-layout">
          <div className="site-content">
            <SiteBreadcrumb location={location} />
            {loading === true && <PageLoadingIndicator loadingText={actionLoading ? "Por favor, aguarde..." : "Carregando informações..."} />}
            {loading === false && error === true && <Alert className="m-b-20" message={"Erro"} description={message} type="error" showIcon />}
            {loading === false && error === false && data.length <= 0 && <Alert className="m-b-20" message={"Sem informações"} description={"Não há dados para exibir no momento..."} type="warning" showIcon closable />}
            {loading === false && (
              <div>
                <h2>Detalhes do orçamento #{data.id} <br /><small style={{ color: '#d9d9d9' }}>{data.order_hash}</small></h2>
                <div style={{ textAlign: 'right' }}>Esse cliente já foi atendido? <Switch checkedChildren="Sim" unCheckedChildren="Não" onChange={() => this.changeOrderInstitutionStatus(data.id, !data.institution_completed)} checked={data.institution_completed == 1 ? true : false} /></div>
                <br />
                <Descriptions title="" bordered size={"middle"}>
                  <Descriptions.Item label="Status" span={3}>
                    <Badge status={data.customer_completed == 1 ? 'success' : 'processing'} text={data.customer_completed == 1 ? 'Preenchimento finalizado' : 'Preenchimento em andamento'} />
                  </Descriptions.Item>
                  <Descriptions.Item label="Tipo de evento" span={2}>{data.order_event_type}</Descriptions.Item>
                  <Descriptions.Item label="Nome do cliente" span={2}>{data.customer_name}</Descriptions.Item>
                  <Descriptions.Item label="E-mail" span={2}>{data.customer_email}</Descriptions.Item>
                  <Descriptions.Item label="Telefone" span={2}>{data.customer_phone}</Descriptions.Item>
                  <Descriptions.Item label="Data do evento" span={2}>{moment(data.event_date).format('DD/MM/YYYY')}</Descriptions.Item>
                  <Descriptions.Item label="Orçamento previsto">{currencyFormatter(data.event_budget)}</Descriptions.Item>
                </Descriptions>
                <br />
                <h2>Serviços/produtos selecionados</h2>
                <ul>
                  {data.order_items.map((element, index) => {
                    return <li key={index} style={{ overflow: 'hidden' }}><div style={{ float: 'left' }}>{element.title}</div> <div style={{ float: 'right' }}>{element.is_free ? <del>{currencyFormatter(element.price)}</del> : currencyFormatter(element.price)}</div></li>
                  })}
                </ul>
                <hr />
                <p style={{ textAlign: 'right' }}>Total do orçamento: {currencyFormatter(data.total_order_value)}</p>
                <h2>Observações do cliente</h2>
                <p>{data.observations}</p>
                <h2>Comentários sobre serviços que ele sentiu falta</h2>
                <p>{data.services_not_offered}</p>
                <Space size={30} style={{ float: 'right' }}>
                  <Button onClick={() => this.goToPreviousPage()} className="action-button-cancel">
                    Voltar para listagem
                  </Button>
                </Space>
              </div>
            )}
          </div>
        </Content>
      </Layout >
    );
  }
}


//Configs Redux para este component
//
//Define quais atributos vou pegar do
//state do Redux
const mapStateToProps = (state) => {
  const { actionFeedbackMessage } = state.systemAlert;
  return { actionFeedbackMessage: actionFeedbackMessage };
};

//Define quais ações esse component
//vai usar para interagir com o Redux
const mapDispatchToProps = (dispatch) => {
  return {
    clearSytemAlert: () => dispatch(clearSytemAlert())
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Details);