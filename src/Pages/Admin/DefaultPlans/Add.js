import React from 'react';
import { connect } from "react-redux";
import { add as addSystemAlert } from "Redux/modules/systemAlert/actions";

import { Layout, Row, Col, Button, Form, Input, InputNumber, Space, Alert, Divider, Tag } from 'antd';
import 'antd/dist/antd.css';
import 'assets/styles/app.css';

import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import '@ckeditor/ckeditor5-build-classic/build/translations/pt-br';

import SiteHeader from 'Pages/Elements/SiteHeader';
import SiteBreadcrumb from 'Pages/Elements/SiteBreadcrumb';
import PageLoadingIndicator from 'Components/PageLoadingIndicator';

import { PlusOutlined } from '@ant-design/icons';

import {currencyFormatter, currencyParser} from 'Utils/functions';

import servicesService from 'Services/services';
import serviceProvidersService from 'Services/serviceProviders';
import serviceCategoriesService from 'Services/serviceCategories';

class Add extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      form: {},
      actionLoading: false,
      error: false,
      message: '',
      showForm: true,
      data: [],
      serviceCategory: { id: null },
      serviceProvider: { id: null },
      editorData: '',
      tags: [],
      inputVisible: false,
      inputValue: '',
      editInputIndex: -1,
      editInputValue: '',
    };
  }


  goTo = (destination) => {
    const { history } = this.props;
    //redirect
    history.push(destination);
  };

  goToPreviousPage = () => {
    const { history } = this.props;
    //redirect
    history.goBack();
  };

  async componentDidMount() {
    await this.loadData();

  }

  loadData = async () => {
    const [servicesProvidersResponse, servicesCategoriesResponse] = await Promise.all([
      serviceProvidersService.findDefault(),
      serviceCategoriesService.findDefault(),
    ]);

    let error = false, message = '', allowSubmit = true, serviceCategory = [], serviceProvider = [];

    if (servicesProvidersResponse.error == false && servicesCategoriesResponse.error == false) {

      if (servicesCategoriesResponse.data && servicesProvidersResponse.data) {
        serviceCategory = servicesCategoriesResponse.data;
        serviceProvider = servicesProvidersResponse.data;
      } else {
        error = true;
        allowSubmit = false;
        message = 'Erro há categorias e/ou fornecedores padrão para cadastrar os planos...';
      }
    } else {
      error = true;
      allowSubmit = false;
      message = 'Erro ao tentar recuperar informações para preenchimento do formulário... Por favor, tente novamente mais tarde...';
    }

    this.setState({ loading: false, error: error, message: message, allowSubmit, serviceCategory, serviceProvider });

  }


  handleSubmit = async (values) => {
    this.setState({ loading: true, actionLoading: true, error: '', form: { ...values } });
    const { title, price } = values;
    const { editorData, serviceCategory, serviceProvider, tags } = this.state;

    if (!title || (!price || price <= 0) || !serviceCategory || !serviceProvider) {
      this.setState({ loading: false, error: true, message: "Preencha um título, um valor, uma categoria e um fornecedor, para continuar..." });
    } else {
      let postData = values;
      postData.description = editorData;
      postData.service_category_id = serviceCategory.id;
      postData.service_provider_id = serviceProvider.id;
      postData.default_attributes = JSON.stringify(tags);
      postData.is_default = 1;
      try {
        const response = await servicesService.add(postData);
        if (response.error == true) {
          this.setState({
            loading: false,
            actionLoading: false,
            error: true,
            message: response.message
          });
        } else {
          await this.props.addSystemAlert({ message: "Plano adicionado com sucesso", type: "success" });
          this.goTo("/painel/planos");
        }
      } catch (err) {
        this.setState({
          loading: false,
          actionLoading: false,
          error: true,
          message: err.message
        });
      }
    }
  };


  handleClose = removedTag => {
    const tags = this.state.tags.filter(tag => tag !== removedTag);
    this.setState({ tags });
  };

  showInput = () => {
    this.setState({ inputVisible: true }, () => this.input.focus());
  };

  handleInputChange = e => {
    this.setState({ inputValue: e.target.value });
  };

  handleInputConfirm = () => {
    const { inputValue } = this.state;
    let { tags } = this.state;
    if (inputValue && tags.indexOf(inputValue) === -1) {
      tags = [...tags, inputValue];
    }
    this.setState({
      tags,
      inputVisible: false,
      inputValue: '',
    });
  };

  handleEditInputChange = e => {
    this.setState({ editInputValue: e.target.value });
  };

  handleEditInputConfirm = () => {
    this.setState(({ tags, editInputIndex, editInputValue }) => {
      const newTags = [...tags];
      newTags[editInputIndex] = editInputValue;

      return {
        tags: newTags,
        editInputIndex: -1,
        editInputValue: '',
      };
    });
  };

  saveInputRef = input => {
    this.input = input;
  };

  saveEditInputRef = input => {
    this.editInput = input;
  };



  render() {
    const { history, location } = this.props;
    const { Content } = Layout;
    const { loading, actionLoading, error, message, allowSubmit, tags, inputVisible, inputValue, editInputIndex, editInputValue } = this.state;

    return (
      <Layout className="main-content">
        <SiteHeader history={history} />
        <Content className="site-layout">
          <div className="site-content">
            <SiteBreadcrumb location={location} />
            {loading === true && <PageLoadingIndicator loadingText={actionLoading ? "Por favor, aguarde..." : "Carregando informações..."} />}
            {loading === false && error === true && <Alert className="m-b-20" message={"Erro"} description={message} type="error" showIcon />}
            {loading === false && (
              <Row>
                <Col span={24}>
                  <Divider orientation="left" plain>
                    Dados básicos
                  </Divider>
                </Col>
                <Col span={24}>
                  <Form
                    layout="vertical"
                    name="serviceProvider"
                    initialValues={{ ...this.state.form }}
                    onFinish={this.handleSubmit}
                  >
                    <Row>
                      <Col span={24}>
                        <Form.Item
                          label="Título"
                          name="title"
                          rules={[{ required: true, message: 'Informe um título para o plano...' }]}
                        >
                          <Input type={"text"} className="default-input" disabled={!allowSubmit} />
                        </Form.Item>
                        <Form.Item
                          label="Resumo do plano"
                          name="headline"
                        >
                          <Input type={"text"} className="default-input" disabled={!allowSubmit} />
                        </Form.Item>
                        <Form.Item
                          label="Descrição completa"
                          name="description"
                        >
                          <CKEditor
                            editor={ClassicEditor}
                            config={{
                              language: 'pt-br',
                              toolbar: ['heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', '|', 'undo', 'redo'],
                            }}
                            onChange={(event, editor) => {
                              const data = editor.getData();
                              this.setState({ editorData: data });
                            }}
                          />
                        </Form.Item>
                        <Form.Item
                          label="Custo"
                          name="price"
                          rules={[{ required: true, message: 'Informe um preço para o plano...' }]}
                        >
                          <InputNumber
                            className="default-input default-input-number"
                            disabled={!allowSubmit}
                            formatter={currencyFormatter}
                            parser={currencyParser}
                          />
                        </Form.Item>
                        <>
                          <Divider className="form-actions-separator" />
                          <h3 className="m-t-20 m-b-20">Características do pacote</h3>
                          {tags.map((tag, index) => {
                            if (editInputIndex === index) {
                              return (
                                <Input
                                  ref={this.saveEditInputRef}
                                  key={tag}
                                  size="small"
                                  className="tag-input"
                                  value={editInputValue}
                                  onChange={this.handleEditInputChange}
                                  onBlur={this.handleEditInputConfirm}
                                  onPressEnter={this.handleEditInputConfirm}
                                />
                              );
                            } else {
                              return (
                                <Tag
                                  className="edit-tag"
                                  key={tag}
                                  closable={true}
                                  onClose={() => this.handleClose(tag)}
                                >
                                  <span
                                    onDoubleClick={e => {
                                      if (index !== 0) {
                                        this.setState({ editInputIndex: index, editInputValue: tag }, () => {
                                          this.editInput.focus();
                                        });
                                        e.preventDefault();
                                      }
                                    }}
                                  >
                                    {tag}
                                  </span>
                                </Tag>
                              );
                            }
                          })}
                          {inputVisible && (
                            <Input
                              ref={this.saveInputRef}
                              type="text"
                              size="small"
                              className="tag-input"
                              value={inputValue}
                              onChange={this.handleInputChange}
                              onBlur={this.handleInputConfirm}
                              onPressEnter={this.handleInputConfirm}
                            />
                          )}
                          {!inputVisible && (
                            <Tag className="site-tag-plus" onClick={this.showInput}>
                              <PlusOutlined /> Adicionar característica
                            </Tag>
                          )}
                        </>
                      </Col>
                    </Row>
                    <Divider className="form-actions-separator" />
                    <Form.Item style={{ float: 'right' }}>
                      <Space size={30}>
                        <Button onClick={() => this.goToPreviousPage()} className="action-button-cancel">
                          {allowSubmit === true ? 'Mudei de ideia' : 'Voltar para página anterior'}
                        </Button>
                        {allowSubmit === true && <Button disabled={loading} className={"action-button"} type="primary" htmlType="submit"> Salvar </Button>}
                      </Space>
                    </Form.Item>
                  </Form>
                </Col>
              </Row>
            )}
          </div>
        </Content>
      </Layout >
    );
  }
}


//Configs Redux para este component
//
//Define quais atributos vou pegar do
//state do Redux
const mapStateToProps = null;

//Define quais ações esse component
//vai usar para interagir com o Redux
const mapDispatchToProps = (dispatch) => {
  return {
    addSystemAlert: (actionFeedbackMessage) => dispatch(addSystemAlert(actionFeedbackMessage))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Add);