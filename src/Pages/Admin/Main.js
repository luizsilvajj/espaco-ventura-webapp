import React from 'react';
import { connect } from "react-redux";
import { Layout, Card, Row, Col, Divider } from 'antd';
import SiteHeader from 'Pages/Elements/SiteHeader';
import 'antd/dist/antd.css';
import 'assets/styles/app.css';

import { clear as clearSytemAlert } from "Redux/modules/systemAlert/actions";
import { flash } from 'Utils/functions';

import { ProfileOutlined, MessageOutlined, TagOutlined, TeamOutlined, GroupOutlined, InboxOutlined, BuildOutlined, ToolOutlined } from '@ant-design/icons';
class Main extends React.Component {


  async componentDidMount() {
    const { actionFeedbackMessage } = this.props;

    if (actionFeedbackMessage) {
      flash(actionFeedbackMessage);
      this.props.clearSytemAlert();
    }

  }

  goTo = (destination) => {
    const { history } = this.props;
    //redirect
    history.push(destination);
  };


  render() {
    const { history } = this.props;
    const { Content } = Layout;
    return (
      <Layout className="main-content">
        <SiteHeader history={history} />
        <Content className="site-layout">
          <div className="admin-main">
            <Divider className="admin-main-divider" orientation="center" plain>
              <span className="admin-section-title">Geral</span>
            </Divider>
            <Row gutter={[24, 24]}>
              <Col xs={24} sm={24} md={12} lg={8} xl={8}>
                <Card hoverable className="admin-main-panel-card" onClick={() => this.goTo('/painel/orcamentos')}>
                  <InboxOutlined className="admin-main-panel-card-icon" />
                  <h3 className="admin-main-panel-card-title">Orçamentos</h3>
                  <p>Orçamento realizados por clientes</p>
                </Card>
              </Col>
            </Row>
            <Divider className="admin-main-divider" orientation="center" plain>
              <span className="admin-section-title">Cadastro Básico</span>
            </Divider>
            <Row gutter={[24, 24]}>
              <Col xs={24} sm={24} md={12} lg={8} xl={8}>
                <Card hoverable className="admin-main-panel-card" onClick={() => this.goTo('/painel/depoimentos')}>
                  <MessageOutlined className="admin-main-panel-card-icon" />
                  <h3 className="admin-main-panel-card-title">Depoimentos</h3>
                  <p>Gerencie os depoimentos dos clientes</p>
                </Card>
              </Col>
              <Col xs={24} sm={24} md={12} lg={8} xl={8}>
                <Card hoverable className="admin-main-panel-card" onClick={() => this.goTo('/painel/tipos-de-evento')}>
                  <TagOutlined className="admin-main-panel-card-icon" />
                  <h3 className="admin-main-panel-card-title">Tipos de Evento</h3>
                  <p>Configure os tipos de evento disponíveis para orçamento</p>
                </Card>
              </Col>
              <Col xs={24} sm={24} md={12} lg={8} xl={8}>
                <Card hoverable className="admin-main-panel-card" onClick={() => this.goTo('/painel/fornecedores')}>
                  <TeamOutlined className="admin-main-panel-card-icon" />
                  <h3 className="admin-main-panel-card-title">Fornecedores</h3>
                  <p>Gerencie os fornecedores dos serviços de planos padrão e personalizados</p>
                </Card>
              </Col>
              <Col xs={24} sm={24} md={12} lg={8} xl={8}>
                <Card hoverable className="admin-main-panel-card" onClick={() => this.goTo('/painel/planos')}>
                  <ProfileOutlined className="admin-main-panel-card-icon" />
                  <h3 className="admin-main-panel-card-title">Planos padrão</h3>
                  <p>Administre os planos padrão que estarão disponíveis no app</p>
                </Card>
              </Col>
              <Col xs={24} sm={24} md={12} lg={8} xl={8}>
                <Card hoverable className="admin-main-panel-card" onClick={() => this.goTo('/painel/categorias')}>
                  <GroupOutlined className="admin-main-panel-card-icon" />
                  <h3 className="admin-main-panel-card-title">Categorias de Serviço</h3>
                  <p>Gerencie as categorias dos serviços de planos personalizados</p>
                </Card>
              </Col>
              <Col xs={24} sm={24} md={12} lg={8} xl={8}>
                <Card hoverable className="admin-main-panel-card" onClick={() => this.goTo('/painel/servicos')}>
                  <BuildOutlined rotate={180} className="admin-main-panel-card-icon" />
                  <h3 className="admin-main-panel-card-title">Serviços</h3>
                  <p>Configure os serviços disponíveis para criação de planos personalizados</p>
                </Card>
              </Col>
              <Col xs={24} sm={24} md={12} lg={8} xl={8}>
                <Card hoverable className="admin-main-panel-card" onClick={() => this.goTo('/painel/configuracoes')}>
                  <ToolOutlined className="admin-main-panel-card-icon" />
                  <h3 className="admin-main-panel-card-title">Configurações do site</h3>
                  <p>Configurações gerais do site</p>
                </Card>
              </Col>
            </Row>
          </div>
        </Content>
      </Layout >
    );
  }
}

//Configs Redux para este component
//
//Define quais atributos vou pegar do
//state do Redux
const mapStateToProps = (state) => {
  const { actionFeedbackMessage } = state.systemAlert;
  return { actionFeedbackMessage: actionFeedbackMessage };
};

//Define quais ações esse component
//vai usar para interagir com o Redux
const mapDispatchToProps = (dispatch) => {
  return {
    clearSytemAlert: () => dispatch(clearSytemAlert())
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);