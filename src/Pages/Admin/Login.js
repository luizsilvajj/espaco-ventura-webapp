import React from 'react';
import { Layout, Row, Col, Button, Form, Input } from 'antd';
import SiteHeader from 'Pages/Elements/SiteHeader';
import LoadingIndicator from 'Components/LoadingIndicator';
import 'antd/dist/antd.css';
import 'assets/styles/app.css';

import authService from "Services/auth";
import { login, getUser } from "Utils/authentication";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      email: "",
      password: "",
      error: ""
    };
    var currentUser = getUser();
    if (currentUser !== null) {
      props.history.push('/painel');
    }
  }

  goToHome = () => {
    const { history } = this.props;

    //redirect
    history.push('/');
  };

  handleSignIn = async (values) => {
    this.setState({ loading: true, error: '' });
    const { email, password } = values;
    if (!email || !password) {
      this.setState({ loading: false, error: "Preencha e-mail e senha para continuar!" });
    } else {
      try {
        const response = await authService.login(email, password);
        if (response.error == true) {
          this.setState({
            loading: false,
            error:
              response.message
          });
        } else {
          login(response.data);
          this.props.history.push("/painel");
        }
      } catch (err) {
        this.setState({
          loading: false,
          error:
            "Login ou senha incorretos"
        });
      }
    }
  };


  render() {

    const { Content } = Layout;
    const { loading } = this.state;
    const { history } = this.props;

    return (
      <Layout className="main-content">
        <SiteHeader history={history} light />
        <Content className="site-layout">
          <Row justify={'center'}>
            <Col xs={24} sm={24} md={16} lg={16} xl={8}>
              <Form
                name="basic"
                style={{ marginTop: 100 }}
                layout={"vertical"}
                onFinish={this.handleSignIn}
              >
                <Form.Item style={{ textAlign: 'center', color: '#b2936d' }}>
                  {this.state.error && <p className="error">{this.state.error}</p>}
                  {loading === true && <LoadingIndicator loadingText={'Verificando credenciais...'} />}
                </Form.Item>
                <Form.Item
                  label="E-mail"
                  name="email"
                  rules={[{ required: true, message: 'Informe seu e-mail...' }]}
                >
                  <Input type={"email"} className="default-input" />
                </Form.Item>

                <Form.Item
                  label="Senha"
                  name="password"
                  rules={[{ required: true, message: 'Informe sua senha...' }]}
                >
                  <Input.Password className="default-input" />
                </Form.Item>

                <Form.Item style={{ overflow: 'hidden' }}>
                  <Button onClick={this.goToHome} className="action-button-cancel" style={{ float: 'left' }}>
                    Mudei de ideia
                  </Button>
                  <Button disabled={loading} className={"action-button"} type="primary" htmlType="submit" style={{ float: 'right' }}>
                    Entrar
                  </Button>
                </Form.Item>
              </Form>
            </Col>
          </Row>
        </Content>
      </Layout >
    );
  }
}

export default Login;