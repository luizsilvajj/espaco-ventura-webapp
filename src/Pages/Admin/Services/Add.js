import React from 'react';
import { connect } from "react-redux";
import { add as addSystemAlert } from "Redux/modules/systemAlert/actions";

import { Layout, Row, Col, Button, Form, Input, InputNumber, Space, Alert, Divider, Select, Checkbox } from 'antd';
import 'antd/dist/antd.css';
import 'assets/styles/app.css';

import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import '@ckeditor/ckeditor5-build-classic/build/translations/pt-br';

import SiteHeader from 'Pages/Elements/SiteHeader';
import SiteBreadcrumb from 'Pages/Elements/SiteBreadcrumb';
import PageLoadingIndicator from 'Components/PageLoadingIndicator';

import {currencyFormatter, currencyParser} from 'Utils/functions';

import servicesService from 'Services/services';
import serviceProvidersService from 'Services/serviceProviders';
import serviceCategoriesService from 'Services/serviceCategories';

class Add extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      form: {},
      actionLoading: false,
      error: false,
      message: '',
      showForm: true,
      data: [],
      editorData: '',
      locale: 'pt-br'
    };
  }


  goTo = (destination) => {
    const { history } = this.props;
    //redirect
    history.push(destination);
  };

  goToPreviousPage = () => {
    const { history } = this.props;
    //redirect
    history.goBack();
  };

  async componentDidMount() {
    await this.loadData();

  }

  loadData = async () => {
    const [servicesProvidersResponse, servicesCategoriesResponse] = await Promise.all([
      serviceProvidersService.findAll(),
      serviceCategoriesService.findAll(),
    ]);

    let error = false, message = '', allowSubmit = true, serviceCategories = [], serviceProviders = [];

    if (servicesProvidersResponse.error == false && servicesCategoriesResponse.error == false) {

      if ((servicesCategoriesResponse.data && servicesCategoriesResponse.data.length > 0) && (servicesProvidersResponse.data && servicesProvidersResponse.data.length > 0)) {
        serviceCategories = servicesCategoriesResponse.data;
        serviceProviders = servicesProvidersResponse.data;
      } else {
        error = true;
        allowSubmit = false;
        message = 'Erro há categorias e/ou fornecedores disponíveis para cadastrar os serviços...';
      }
    } else {
      error = true;
      allowSubmit = false;
      message = 'Erro ao tentar recuperar informações para preenchimento do formulário... Por favor, tente novamente mais tarde...';
    }

    this.setState({ loading: false, error: error, message: message, allowSubmit, serviceCategories, serviceProviders });

  }


  handleSubmit = async (values) => {
    this.setState({ loading: true, actionLoading: true, error: '', form: { ...values } });
    const { title, price, service_category_id, service_provider_id } = values;
    const { editorData } = this.state;
    
    if (!title || (!price || price <= 0) || !service_category_id || !service_provider_id) {
      this.setState({ loading: false, error: true, message: "Preencha um título, um valor, uma categoria e um fornecedor, para continuar..." });
    } else {
      let postData = values;
      postData.description = editorData;
      try {
        const response = await servicesService.add(postData);
        if (response.error == true) {
          this.setState({
            loading: false,
            actionLoading: false,
            error: true,
            message: response.message
          });
        } else {
          await this.props.addSystemAlert({ message: "Serviço adicionado com sucesso", type: "success" });
          this.goTo("/painel/servicos");
        }
      } catch (err) {
        this.setState({
          loading: false,
          actionLoading: false,
          error: true,
          message: err.message
        });
      }
    }
  };


  render() {
    const { history, location } = this.props;
    const { Content } = Layout;
    const { loading, actionLoading, error, message, allowSubmit, serviceCategories, serviceProviders } = this.state;

    return (
      <Layout className="main-content">
        <SiteHeader history={history} />
        <Content className="site-layout">
          <div className="site-content">
            <SiteBreadcrumb location={location} />
            {loading === true && <PageLoadingIndicator loadingText={actionLoading ? "Por favor, aguarde..." : "Carregando informações..."} />}
            {loading === false && error === true && <Alert className="m-b-20" message={"Erro"} description={message} type="error" showIcon />}
            {loading === false && (
              <Row>
                <Col span={24}>
                  <Divider orientation="left" plain>
                    Dados básicos
                  </Divider>
                </Col>
                <Col span={24}>
                  <Form
                    layout="vertical"
                    name="serviceProvider"
                    initialValues={{ ...this.state.form }}
                    onFinish={this.handleSubmit}
                  >
                    <Row>
                      <Col span={24}>
                        {serviceCategories.length > 0 &&
                          <Form.Item label="Categoria" name="service_category_id">
                            <Select
                              rules={[{ required: true, message: 'Informe uma categoria para o serviço...' }]}
                              disabled={!allowSubmit}
                            >
                              {serviceCategories.map((category, index) => <Select.Option key={category.id} value={category.id}>{category.title}</Select.Option>)}
                            </Select>
                          </Form.Item>
                        }
                        {serviceProviders.length > 0 &&
                          <Form.Item label="Fornecedor" name="service_provider_id">
                            <Select
                              rules={[{ required: true, message: 'Informe um fornecedor para o serviço...' }]}
                              disabled={!allowSubmit}
                            >
                              {serviceProviders.map((provider, index) => <Select.Option key={provider.id} value={provider.id}>{provider.name}</Select.Option>)}
                            </Select>
                          </Form.Item>
                        }
                        <Form.Item
                          label="Título"
                          name="title"
                          rules={[{ required: true, message: 'Informe um título para o serviço...' }]}
                        >
                          <Input type={"text"} className="default-input" disabled={!allowSubmit} />
                        </Form.Item>
                        <Form.Item
                          label="Resumo do serviço"
                          name="headline"
                        >
                          <Input type={"text"} className="default-input" disabled={!allowSubmit} />
                        </Form.Item>
                        <Form.Item
                          label="Descrição completa"
                          name="description"
                        >
                          <CKEditor
                            editor={ClassicEditor}
                            config={{
                              language: 'pt-br',
                              toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', '|', 'undo', 'redo' ],
                            }}
                            onChange={(event, editor) => {
                              const data = editor.getData();
                              this.setState({ editorData: data });
                            }}
                          />
                        </Form.Item>
                        <Form.Item
                          label="Custo"
                          name="price"
                          rules={[{ required: true, message: 'Informe um preço para o serviço...' }]}
                        >
                          <InputNumber
                            className="default-input default-input-number"
                            disabled={!allowSubmit}
                            formatter={currencyFormatter}
                            parser={currencyParser}
                          />
                        </Form.Item>
                        <Form.Item label="Serviço gratuito / cortesia" name="is_free" valuePropName="checked">
                          <Checkbox>Não cobrar o custo do serviço</Checkbox>
                        </Form.Item>
                      </Col>
                    </Row>
                    <Divider className="form-actions-separator" />
                    <Form.Item style={{ float: 'right' }}>
                      <Space size={30}>
                        <Button onClick={() => this.goToPreviousPage()} className="action-button-cancel">
                          {allowSubmit === true ? 'Mudei de ideia' : 'Voltar para página anterior'}
                        </Button>
                        {allowSubmit === true && <Button disabled={loading} className={"action-button"} type="primary" htmlType="submit"> Salvar </Button>}
                      </Space>
                    </Form.Item>
                  </Form>
                </Col>
              </Row>
            )}
          </div>
        </Content>
      </Layout >
    );
  }
}


//Configs Redux para este component
//
//Define quais atributos vou pegar do
//state do Redux
const mapStateToProps = null;

//Define quais ações esse component
//vai usar para interagir com o Redux
const mapDispatchToProps = (dispatch) => {
  return {
    addSystemAlert: (actionFeedbackMessage) => dispatch(addSystemAlert(actionFeedbackMessage))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Add);