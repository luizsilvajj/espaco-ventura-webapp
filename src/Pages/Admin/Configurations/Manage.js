import React from 'react';
import { connect } from "react-redux";
import { add as addSystemAlert } from "Redux/modules/systemAlert/actions";

import { Layout, Row, Col, Button, Form, Input, Space, Alert, Divider } from 'antd';
import 'antd/dist/antd.css';
import 'assets/styles/app.css';

import SiteHeader from 'Pages/Elements/SiteHeader';
import SiteBreadcrumb from 'Pages/Elements/SiteBreadcrumb';
import PageLoadingIndicator from 'Components/PageLoadingIndicator';

import { validatePhoneInput, handleResultData } from 'Utils/functions';

import configurationsService from 'Services/configurations';

import { CONFIG_COMMERCIAL_NUMBER, HOME_INDICATOR_FACEBOOK_RATING, HOME_INDICATOR_GOOGLE_COMMENTS_RATING, HOME_INDICATOR_TOTAL_EVENTS } from 'Config/constants';

class Manage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      form: {},
      actionLoading: false,
      error: false,
      message: '',
      data: [],
    };
  }

  async componentDidMount() {
    await this.loadData();
  }

  loadData = async () => {
    const [totalEventsResponse, facebookRatingResponse, googleCommentsRatingResponse, commercialNumberResponse] = await Promise.all([
      configurationsService.findBySlug(HOME_INDICATOR_TOTAL_EVENTS),
      configurationsService.findBySlug(HOME_INDICATOR_FACEBOOK_RATING),
      configurationsService.findBySlug(HOME_INDICATOR_GOOGLE_COMMENTS_RATING),
      configurationsService.findBySlug(CONFIG_COMMERCIAL_NUMBER),
    ]);

    let contentError = false, error = false, message = '', allowSubmit = true, totalEvents = '', facebookRating = '', googleCommentsRating = '', commercialNumber = '';

    if (totalEventsResponse.error == true) {
      if (totalEventsResponse.error_code == 404) {
        let newItem = { slug: HOME_INDICATOR_TOTAL_EVENTS, content: '', name: 'Indicador de total de eventos realizados da home' };
        configurationsService.add(newItem).then((result) => {
          let handledResult = handleResultData(result);
          if (handledResult.error == true) {
            this.setState({ ...handledResult, loading: false });
          }
        });
      } else {
        contentError = true;
      }
    } else {
      totalEvents = totalEventsResponse.data.content;
    }

    if (facebookRatingResponse.error == true) {
      if (facebookRatingResponse.error_code == 404) {
        let newItem = { slug: HOME_INDICATOR_FACEBOOK_RATING, content: '', name: 'Indicador de classificação no Facebook da home' };
        configurationsService.add(newItem).then((result) => {
          let handledResult = handleResultData(result);
          if (handledResult.error == true) {
            this.setState({ ...handledResult, loading: false });
          }
        });
      } else {
        contentError = true;
      }
    } else {
      facebookRating = facebookRatingResponse.data.content;
    }

    if (googleCommentsRatingResponse.error == true) {
      if (googleCommentsRatingResponse.error_code == 404) {
        let newItem = { slug: HOME_INDICATOR_GOOGLE_COMMENTS_RATING, content: '', name: 'Indicador de classificação de comentários do Google da home' };
        configurationsService.add(newItem).then((result) => {
          let handledResult = handleResultData(result);
          if (handledResult.error == true) {
            this.setState({ ...handledResult, loading: false });
          }
        });
      } else {
        contentError = true;
      }
    } else {
      googleCommentsRating = googleCommentsRatingResponse.data.content;
    }

    if (commercialNumberResponse.error == true) {
      if (commercialNumberResponse.error_code == 404) {
        let newItem = { slug: CONFIG_COMMERCIAL_NUMBER, content: '', name: 'Número de telefone do whatsapp' };
        configurationsService.add(newItem).then((result) => {
          let handledResult = handleResultData(result);
          if (handledResult.error == true) {
            this.setState({ ...handledResult, loading: false });
          }
        });
      } else {
        contentError = true;
      }
    } else {
      commercialNumber = commercialNumberResponse.data.content;
    }



    this.setState({ loading: false, error: error, message: message, allowSubmit: allowSubmit, form: { totalEvents: totalEvents, facebookRating: facebookRating, googleCommentsRating: googleCommentsRating, commercialNumber: commercialNumber } });

  }

  goTo = (destination) => {
    const { history } = this.props;
    //redirect
    history.push(destination);
  };

  goToPreviousPage = () => {
    const { history } = this.props;
    //redirect
    history.goBack();
  };


  handleSubmit = async (values) => {
    this.setState({ loading: true, actionLoading: true, error: '', form: { ...values } });
    const { totalEvents,
      facebookRating,
      googleCommentsRating,
      commercialNumber } = values;
    if (!(totalEvents && facebookRating && googleCommentsRating && commercialNumber)) {
      this.setState({ loading: false, error: "Preencha todos os campos para continuar..." });
    } else {

      try {
        const [totalEventsResponse, facebookRatingResponse, googleCommentsRatingResponse, commercialNumberResponse] = await Promise.all([
          configurationsService.update(HOME_INDICATOR_TOTAL_EVENTS, { content: totalEvents }),
          configurationsService.update(HOME_INDICATOR_FACEBOOK_RATING, { content: facebookRating }),
          configurationsService.update(HOME_INDICATOR_GOOGLE_COMMENTS_RATING, { content: googleCommentsRating }),
          configurationsService.update(CONFIG_COMMERCIAL_NUMBER, { content: commercialNumber }),
        ]);

        if (totalEventsResponse.error == true ||
          facebookRatingResponse.error == true ||
          googleCommentsRatingResponse.error == true ||
          commercialNumberResponse.error == true) {
          this.setState({
            loading: false,
            actionLoading: false,
            error: true,
            message: 'Erro ao tentar salvar as configurações...'
          });
        } else {
          await this.props.addSystemAlert({ message: "Configurações atualizadas com sucesso", type: "success" });
          this.goTo("/painel");
        }
      } catch (err) {
        this.setState({
          loading: false,
          actionLoading: false,
          error: true,
          message: err.message
        });
      }
    }
  };


  render() {
    const { history, location } = this.props;
    const { Content } = Layout;
    const { loading, actionLoading, error, message } = this.state;


    return (
      <Layout className="main-content">
        <SiteHeader history={history} />
        <Content className="site-layout">
          <div className="site-content">
            <SiteBreadcrumb location={location} />
            {loading === true && <PageLoadingIndicator loadingText={actionLoading ? "Por favor, aguarde..." : "Carregando informações..."} />}
            {loading === false && error === true && <Alert className="m-b-20" message={"Erro"} description={message} type="error" showIcon />}
            {loading === false && (
              <Row>
                <Col span={24}>
                  <Divider orientation="left" plain>
                    Dados básicos
                  </Divider>
                </Col>
                <Col span={24}>
                  <Form
                    layout="vertical"
                    name="configurations"
                    initialValues={{ ...this.state.form }}
                    onFinish={this.handleSubmit}
                  >
                    <Row gutter={16}>
                      <Col span={8}>
                        <Form.Item
                          label="Total de eventos realizados (home)"
                          name="totalEvents"
                          rules={[{ required: true, message: 'Quantidade de eventos realizados (exibido na Home)' }]}
                        >
                          <Input type={"text"} className="default-input" />
                        </Form.Item>
                      </Col>
                      <Col span={8}>
                        <Form.Item
                          label="Classificação Facebook (home)"
                          name="facebookRating"
                          rules={[{ required: true, message: 'Classificação no facebook (exibido na Home)' }]}
                        >
                          <Input type={"text"} className="default-input" />
                        </Form.Item>
                      </Col>
                      <Col span={8}>
                        <Form.Item
                          label="Classificação nos comentários do Google (home)"
                          name="googleCommentsRating"
                          rules={[{ required: true, message: 'Classificação nos comentários do Google (exibido na Home)' }]}
                        >
                          <Input type={"text"} className="default-input" />
                        </Form.Item>
                      </Col>
                      <Col span={8}>
                        <Form.Item
                          label="Número do Whatsapp (finalização de orçamento)"
                          name="commercialNumber"
                          onKeyPress={validatePhoneInput}
                          rules={[{ required: true, message: 'Número de telefone pra onde os clientes serão direcionados' }]}
                        >
                          <Input type={"text"} className="default-input" />
                        </Form.Item>
                      </Col>
                    </Row>
                    <Divider className="form-actions-separator" />
                    <Form.Item style={{ float: 'right' }}>
                      <Space size={30}>
                        <Button onClick={() => this.goToPreviousPage()} className="action-button-cancel">
                          Mudei de ideia
                        </Button>
                        <Button disabled={loading} className={"action-button"} type="primary" htmlType="submit">
                          Salvar
                        </Button>
                      </Space>
                    </Form.Item>
                  </Form>
                </Col>
              </Row>
            )}
          </div>
        </Content>
      </Layout >
    );
  }
}


//Configs Redux para este component
//
//Define quais atributos vou pegar do
//state do Redux
const mapStateToProps = null;

//Define quais ações esse component
//vai usar para interagir com o Redux
const mapDispatchToProps = (dispatch) => {
  return {
    addSystemAlert: (actionFeedbackMessage) => dispatch(addSystemAlert(actionFeedbackMessage))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Manage);