import React from 'react';
import { connect } from "react-redux";
import { add as addSystemAlert } from "Redux/modules/systemAlert/actions";
import moment from 'moment';
import 'moment/locale/pt-br';
import locale from 'antd/es/date-picker/locale/pt_BR';

import { Layout, Row, Col, Button, Form, Input, Space, Alert, Divider, Upload, DatePicker } from 'antd';
import ImgCrop from 'antd-img-crop';
import 'antd/dist/antd.css';
import 'assets/styles/app.css';

import SiteHeader from 'Pages/Elements/SiteHeader';
import SiteBreadcrumb from 'Pages/Elements/SiteBreadcrumb';
import PageLoadingIndicator from 'Components/PageLoadingIndicator';


import testimonialsService from 'Services/testimonials';

class Edit extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      form: {},
      actionLoading: false,
      error: false,
      message: '',
      data: [],
      fileList: [],
      removeAvatar: false,
    };
  }

  componentDidMount() {
    const { id } = this.props.match.params;
    this.loadData(id);
  }

  loadData = async (id) => {
    await testimonialsService.findById(id).then((result) => {
      let { error, message, data } = result;
      let fileList = [];
      if (data.customer_picture) {
        fileList.push({
          uid: '-1',
          name: 'customer_picture.png',
          status: 'done',
          url: data.customer_picture
        });
      }

      if (data.testimonial_date) {
        data.testimonial_date = moment(data.testimonial_date);
      }

      this.setState({ error: error, message: message, form: data, fileList: fileList, loading: false });
    });
  }

  goTo = (destination) => {
    const { history } = this.props;
    //redirect
    history.push(destination);
  };

  goToPreviousPage = () => {
    const { history } = this.props;
    //redirect
    history.goBack();
  };

  normFile = (e) => {
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  localUpload = ({ file, onSuccess }) => {
    setTimeout(() => {
      onSuccess("ok");
    }, 0);
  };

  onChange = (newFileList) => {
    const fileListToSave = newFileList.fileList || [];
    const removeAvatar = fileListToSave.length > 0 ? false : true;
    this.setState({ fileList: fileListToSave, removeAvatar });
  };

  onPreview = async file => {
    let src = file.url;
    if (!src) {
      src = await new Promise(resolve => {
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj);
        reader.onload = () => resolve(reader.result);
      });
    }
    const image = new Image();
    image.src = src;
    const imgWindow = window.open(src);
    imgWindow.document.write(image.outerHTML);
  };

  handleSubmit = async (values) => {
    this.setState({ loading: true, actionLoading: true, error: '', form: { ...values } });
    const { id } = this.props.match.params;
    const { fileList, removeAvatar } = this.state;
    const { customer_name, content } = values;
    if (!(customer_name && content)) {
      this.setState({ loading: false, error: "Preencha o nome do cliente e/ou o conteúdo do depoimento para continuar..." });
    } else {
      let postData = values;
      postData.fileList = fileList;
      postData.remove_avatar = removeAvatar;
      if (postData.testimonial_date) {
        postData.testimonial_date = moment(postData.testimonial_date).format("YYYY-MM-DD");
      }
      try {
        const response = await testimonialsService.update(id, postData);
        if (response.error == true) {
          this.setState({
            loading: false,
            actionLoading: false,
            error: true,
            message: response.message
          });
        } else {
          await this.props.addSystemAlert({ message: "Depoimento atualizado com sucesso", type: "success" });
          this.goTo("/painel/depoimentos");
        }
      } catch (err) {
        this.setState({
          loading: false,
          actionLoading: false,
          error: true,
          message: err.message
        });
      }
    }
  };


  render() {
    const { history, location } = this.props;
    const { Content } = Layout;
    const { loading, actionLoading, error, message, fileList } = this.state;

    const { onChange, onPreview, normFile, localUpload } = this;
    

    return (
      <Layout className="main-content">
        <SiteHeader history={history} />
        <Content className="site-layout">
          <div className="site-content">
            <SiteBreadcrumb location={location} />
            {loading === true && <PageLoadingIndicator loadingText={actionLoading ? "Por favor, aguarde..." : "Carregando informações..."} />}
            {loading === false && error === true && <Alert className="m-b-20" message={"Erro"} description={message} type="error" showIcon />}
            {loading === false && (
              <Row>
                <Col span={24}>
                  <Divider orientation="left" plain>
                    Dados básicos
                  </Divider>
                </Col>
                <Col span={24}>
                  <Form
                    layout="vertical"
                    name="testimonial"
                    initialValues={{ ...this.state.form }}
                    onFinish={this.handleSubmit}
                  >
                    <Row>
                      <Col span={24}>
                        <Form.Item
                          label="Nome"
                          name="customer_name"
                          rules={[{ required: true, message: 'Informe o nome do cliente...' }]}
                        >
                          <Input type={"text"} className="default-input" />
                        </Form.Item>
                        <Form.Item
                          label="Foto"
                          name="customer_picture"
                          valuePropName="customer_picture"
                          getValueFromEvent={normFile}
                        >
                          <ImgCrop modalTitle={"Editar imagem"} modalCancel={"Mudei de ideia"} modalOk={"Enviar"} rotate>
                            <Upload
                              listType="picture-card"
                              fileList={fileList}
                              name={'customer_picture'}
                              onChange={onChange}
                              onPreview={onPreview}
                              customRequest={localUpload}
                            >
                              {fileList.length < 1 && '+ Escolher'}
                            </Upload>
                          </ImgCrop>
                        </Form.Item>
                        <Form.Item
                          label="Data"
                          name="testimonial_date"
                        >
                          <DatePicker locale={locale} placeholder={"Escolha uma data"} format={"DD/MM/YYYY"} className="default-input" />
                        </Form.Item>
                        <Form.Item
                          label="Depoimento"
                          name="content"
                          rules={[{ required: true, message: 'Informe o conteúdo do depoimento do cliente...' }]}
                        >
                          <Input.TextArea className="default-input" autoSize={{ minRows: 4 }} maxLength={2000} showCount />
                        </Form.Item>
                      </Col>
                    </Row>
                    <Divider className="form-actions-separator" />
                    <Form.Item style={{ float: 'right' }}>
                      <Space size={30}>
                        <Button onClick={() => this.goToPreviousPage()} className="action-button-cancel">
                          Mudei de ideia
                        </Button>
                        <Button disabled={loading} className={"action-button"} type="primary" htmlType="submit">
                          Salvar
                        </Button>
                      </Space>
                    </Form.Item>
                  </Form>
                </Col>
              </Row>
            )}
          </div>
        </Content>
      </Layout >
    );
  }
}


//Configs Redux para este component
//
//Define quais atributos vou pegar do
//state do Redux
const mapStateToProps = null;

//Define quais ações esse component
//vai usar para interagir com o Redux
const mapDispatchToProps = (dispatch) => {
  return {
    addSystemAlert: (actionFeedbackMessage) => dispatch(addSystemAlert(actionFeedbackMessage))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Edit);