import React from 'react';
import { Layout, Row, Col, Button, Steps, Alert, Select, InputNumber } from 'antd';

import SiteHeader from 'Pages/Elements/SiteHeader';
import 'antd/dist/antd.css';
import 'assets/styles/app.css';
import styles from 'assets/styles/styles';

import PageLoadingIndicator from 'Components/PageLoadingIndicator';

import moment from 'moment';
import 'moment/locale/pt-br';

import { handleResultData } from 'Utils/functions';
import { hasOrder, getOrderHash } from 'Utils/public-order';

import ordersService from 'Services/orders';


class CustomerCompleteOrder extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      error: false,
      formError: false,
      message: '',
      formMessage: '',
      orderId: '',
      orderHash: '',
      customerFirstName: '',
      sno: '',
      ob: '',
    };
    this.loadData = this.loadData.bind(this);
  }

  async componentDidMount() {
    await this.loadData();
  }

  loadData = async () => {
    const { history } = this.props;
    //Carrega as informações do Pedido
    let hasLocalOrder = hasOrder();

    if (hasLocalOrder) {
      const orderHash = getOrderHash();
      ordersService.publicFindByHash(orderHash).then((result) => {
        let handledResult = handleResultData(result);
        if (handledResult.error == true) {
          history.push('novo-evento');
        } else {
          this.setState({
            orderId: handledResult.data.id,
            orderHash: handledResult.data.order_hash,
            loading: false
          });
        }
      });
    } else {
      history.push('novo-evento');
    }
  }


  onChange = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  submit = async () => {
    const { history } = this.props;
    //TODO - submit do form
    const { sno, ob, orderId, orderHash } = this.state;
    let dateTime = moment().format('YYYY-MM-DD HH:mm:ss');
    let params = {
      services_not_offered: sno,
      observations: ob,
      customer_completed: 1,
      modified: dateTime,
      completed: dateTime
    };

    this.setState({ loading: true });
    await ordersService.publicUpdate(orderId, params).then((result) => {
      let handledResult = handleResultData(result);
      if (handledResult.error == true) {
        this.setState({ formError: true, formMessage: handledResult.message, loading: false });
      } else {
        //redirect
        history.push('/orcamento/' + orderHash);
      }
    });
  };

  render() {
    const { history } = this.props;

    const { Content } = Layout;
    const { Step } = Steps;

    const { loading, error, message, formError, formMessage, customerFirstName } = this.state;

    return (
      <Layout className="main-content">
        {loading === true && <PageLoadingIndicator loadingText="Carregando informações..." />}
        {loading === false && error === true && <Alert className="m-b-20" message={"Erro"} description={message} type="error" showIcon />}
        {loading === false && error === false && (
          <div>
            <SiteHeader history={history} />
            <Content className="site-layout">
              <Steps style={styles.steps} size="small" current={2}>
                <Step title="Novo evento" />
                <Step title="Quanto custa" />
                <Step title="Finalize" />
              </Steps>
              <Row>
                <Col className="basic-data-intro" span={24}>
                  <h2 className="highlight">Estamos quase lá{customerFirstName ? ", " + customerFirstName : ""}!</h2>
                  <p>Já tenho quase todas as informações que preciso pra montar seu orçamento, mas antes de finalizar...</p>
                </Col>
              </Row>
            </Content>
            <div className="basic-data-content-container alt site-layout">
              <Content>
                <Row>
                  <Col span={24}>
                    <div className="basic-data-container last">
                      <div className="basic-data-fields-container">
                        <div className="basic-data-field">
                          <label className="basic-data-form-label">Sentiu falta de alguma coisa? Qual serviço você gostaria, mas não encontrou na nossa lista?</label>
                          <input
                            className="basic-data-form-input-text"
                            name="sno"
                            value={this.state.sno || ""}
                            onChange={this.onChange}
                            type="text"
                            autoComplete="off" />
                        </div>
                        <div className="basic-data-field m-t-20">
                          <label className="basic-data-form-label">Quer fazer algum comentário/observação?</label>
                          <input
                            className="basic-data-form-input-text"
                            name="ob"
                            value={this.state.ob || ""}
                            onChange={this.onChange}
                            type="text"
                            autoComplete="off" />
                        </div>
                      </div>
                    </div>
                  </Col>
                  <Col style={{ textAlign: 'right' }} span={24}>
                    <Button onClick={this.submit} className="action-button" size={'large'} type={"primary"}> Finalizar </Button>
                  </Col>
                </Row>
              </Content>
            </div>
          </div>
        )}
      </Layout >
    );
  }
}

export default CustomerCompleteOrder;