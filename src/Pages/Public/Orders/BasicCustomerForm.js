import React from 'react';
import { Layout, Row, Col, Button, Steps, Alert, Select, InputNumber, DatePicker } from 'antd';

import SiteHeader from 'Pages/Elements/SiteHeader';
import 'antd/dist/antd.css';
import 'assets/styles/app.css';
import styles from 'assets/styles/styles';

import PageLoadingIndicator from 'Components/PageLoadingIndicator';

import moment from 'moment';
import 'moment/locale/pt-br';
import locale from 'antd/es/date-picker/locale/pt_BR';

import ReactInputDateMask from 'react-input-date-mask';

import { handleResultData, currencyFormatter, currencyParser, validatePhoneInput } from 'Utils/functions';
import { hasOrder, getOrderHash, registerNewOrder } from 'Utils/public-order';

import ordersService from 'Services/orders';
import eventTypesService from 'Services/eventTypes';


class BasicCustomerForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      error: false,
      formError: false,
      message: '',
      formMessage: '',
      orderId: '',
      orderHash: '',
      eventTypes: [],
      un: '', //user_name    - Nome
      ue: '', //user_email   - Email
      up: '', //user_phone   - Telefone
      et: '', //event_type   - Tipo do evento
      ed: moment(), //event_date   - Data do evento
      eb: '', //event_budget - Orçamento
    };
    this.loadData = this.loadData.bind(this);
  }

  async componentDidMount() {
    await this.loadData();
  }

  loadData = async () => {

    //Carrega os tipos de serviço
    await eventTypesService.findAll().then((eventTypeResult) => {
      let eventTypeHandledResult = handleResultData(eventTypeResult);
      if (eventTypeHandledResult.error == true) {
        this.setState({ ...eventTypeHandledResult, loading: false });
      } else {

        this.setState({ eventTypes: eventTypeHandledResult.data });

        //Carrega as informações do Pedido
        let hasLocalOrder = hasOrder();

        if (hasLocalOrder) {
          const orderHash = getOrderHash();
          ordersService.publicFindByHash(orderHash).then((result) => {
            let handledResult = handleResultData(result);
            if (handledResult.error == true) {
              if (handledResult.error_code == 404) {
                ordersService.createHash().then((result) => {
                  let handledResult = handleResultData(result);
                  if (handledResult.error == true) {
                    this.setState({ ...handledResult, loading: false });
                  } else {
                    registerNewOrder(handledResult.data);
                    this.setState({ orderId: handledResult.data.id, orderHash: handledResult.data.order_hash, loading: false });
                  }
                });
              } else {
                this.setState({ ...handledResult, loading: false });
              }
            } else {
              const { customer_name: un, customer_email: ue, customer_phone: up, event_type_id: et, event_budget: eb, event_date: ed } = handledResult.data;

              this.setState({
                orderId: handledResult.data.id,
                orderHash: handledResult.data.order_hash,
                un: un, //user_name    - Nome
                ue: ue, //user_email   - Email
                up: up, //user_phone   - Telefone
                et: et, //event_type   - Tipo do evento
                ed: moment(ed).format('DD/MM/YYYY'), //event_date   - Data do evento
                eb: eb, //event_budget - Orçamento
                loading: false
              });
            }
          });
        } else {
          ordersService.createHash().then((result) => {
            let handledResult = handleResultData(result);
            if (handledResult.error == true) {
              this.setState({ ...handledResult, loading: false });
            } else {
              registerNewOrder(handledResult.data);
              this.setState({ orderId: handledResult.data.id, orderHash: handledResult.data.order_hash, loading: false });
            }
          });
        }
      }
    });
  }


  onChange = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  onChangeSelect = (value) => {
    this.setState({ et: value });
  }

  onChangeNumber = (value) => {
    this.setState({ eb: value });
  }

  onChangeDate = (value) => {
    this.setState({ ed: value });
  }


  submit = async () => {
    const { history } = this.props;
    const { un, ue, up, et, eb, ed, orderId } = this.state;

    let eventDate = moment(ed, 'DD/MM/YYYY', true);

    let numberIsValid = eb <= 99999999.99;
    if (eventDate.isValid()) {
      if(numberIsValid){
        let params = {
          event_type_id: et,
          event_budget: eb,
          event_date: eventDate.format("YYYY-MM-DD"),
          customer_name: un,
          customer_phone: up,
          customer_email: ue,
          modified: moment().format('YYYY-MM-DD HH:mm:ss')
        };
  
        this.setState({ loading: true });
        await ordersService.publicUpdate(orderId, params).then((result) => {
          let handledResult = handleResultData(result);
          if (handledResult.error == true) {
            this.setState({ formError: true, formMessage: handledResult.message, loading: false });
          } else {
            //redirect
            history.push('planos');
          }
        });
      } else {
        this.setState({ formError: true, formMessage: 'O valor informado é inválido...', loading: false });
      }
    } else {
      this.setState({ formError: true, formMessage: 'A data informada é inválida...', loading: false });
    }

  };

  thisFieldIsEnabled = (field) => {
    let output = true;

    const { un, ue, up, et, ed, eb } = this.state;

    switch (field) {
      case 'ue': {
        output = un ? true : false;
      } break;
      case 'up': {
        output = un && ue ? true : false;
      } break;
      case 'et': {
        output = un && ue && up ? true : false;
      } break;
      case 'ed': {
        output = un && ue && up && et ? true : false;
      } break;
      case 'eb': {
        output = un && ue && up && et && ed ? true : false;
      } break;
      case 'submit-action': {
        output = un && ue && up && et && ed && eb ? true : false;
      } break;
    }

    return output;
  }

  render() {
    const { history } = this.props;

    const { Content } = Layout;
    const { Step } = Steps;

    const { thisFieldIsEnabled } = this;

    const { loading, error, message, formError, formMessage, eventTypes } = this.state;

    let validDate = moment(this.state.ed, "DD/MM/YYYY", true).isValid() ? moment(this.state.ed, "DD/MM/YYYY", true) : ''; 

    return (
      <Layout className="main-content">
        {loading === true && <PageLoadingIndicator loadingText="Carregando informações..." />}
        {loading === false && error === true && <Alert className="m-b-20" message={"Erro"} description={message} type="error" showIcon />}
        {loading === false && error === false && (
          <div>
            <SiteHeader history={history} />
            <Content className="site-layout">
              <Steps style={styles.steps} size="small" current={0}>
                <Step title="Novo evento" />
                <Step title="Quanto custa" />
                <Step title="Finalize" />
              </Steps>
              <Row>
                <Col className="basic-data-intro" span={24}>
                  <h2 className="highlight">Podemos oferecer todas as soluções</h2>
                  <p>Se quiser alugar somente o espaço ou fazer uma festa completa, a gente te ajuda...</p>
                </Col>
              </Row>
            </Content>
            <div className="basic-data-content-container site-layout">
              <Content>
                <Row>
                  <Col span={24}>
                    {loading === false && formError === true && <Alert className="m-b-20" message={"Erro"} description={formMessage} type="error" showIcon />}
                    <div className="basic-data-container">
                      <div className="basic-data-fields-container">
                        <h3 className="basic-data-title text-center">Fale de você...</h3>
                        <div className="basic-data-field">
                          <label className="basic-data-form-label">Qual é o seu nome?</label>
                          <input
                            className="basic-data-form-input-text"
                            name="un"
                            value={this.state.un || ""}
                            onChange={this.onChange}
                            type="text"
                            autoComplete="off" />
                        </div>
                        <div className={thisFieldIsEnabled('ue') ? "basic-data-field m-t-20" : "basic-data-field m-t-20 inactive"}>
                          <label className="basic-data-form-label">Qual é o seu e-mail?</label>
                          <input
                            className="basic-data-form-input-text input-email"
                            name="ue"
                            value={this.state.ue || ""}
                            onChange={this.onChange}
                            type="text"
                            autoComplete="off"
                            disabled={!thisFieldIsEnabled('ue')} />
                        </div>
                        <div className={thisFieldIsEnabled('up') ? "basic-data-field m-t-20" : "basic-data-field m-t-20 inactive"}>
                          <label className="basic-data-form-label">Qual é o seu telefone?</label>
                          <input
                            className="basic-data-form-input-text"
                            name="up"
                            value={this.state.up || ""}
                            onChange={this.onChange}
                            type="text"
                            autoComplete="off"
                            disabled={!thisFieldIsEnabled('up')}
                            onKeyPress={validatePhoneInput}
                          />
                        </div>
                      </div>
                    </div>
                  </Col>
                </Row>
              </Content>
            </div>
            <div className="basic-data-content-container alt site-layout">
              <Content>
                <Row>
                  <Col span={24}>
                    <div className="basic-data-container last">
                      <div className="basic-data-fields-container">
                        <h3 className={thisFieldIsEnabled('et') ? "basic-data-title text-center" : "basic-data-title text-center inactive"} >Já pensou no tipo de evento e no seu orçamento?</h3>
                        <div className={thisFieldIsEnabled('et') ? "basic-data-field" : "basic-data-field inactive"}>
                          <label className="basic-data-form-label">Qual é o tipo de evento?</label>
                          <Select className="basic-data-form-input-text"
                            bordered={false}
                            name="et"
                            value={this.state.et || ""}
                            onChange={this.onChangeSelect}
                            type="text"
                            autoComplete="off"
                            disabled={!thisFieldIsEnabled('et')}>
                            {eventTypes.map((eventType, index) => <Select.Option key={eventType.id} value={eventType.id}>{eventType.title}</Select.Option>)}
                          </Select>
                        </div>
                        <div className={thisFieldIsEnabled('ed') ? "basic-data-field m-t-20" : "basic-data-field m-t-20 inactive"}>
                          <label className="basic-data-form-label">Qual é a data do evento?</label>
                            <DatePicker locale={locale} onChange={this.onChangeDate} format={"DD/MM/YYYY"} className="basic-data-form-input-text" defaultValue={validDate} disabled={!thisFieldIsEnabled('ed')}/>
                        </div>
                        <div className={thisFieldIsEnabled('eb') ? "basic-data-field m-t-20" : "basic-data-field m-t-20 inactive"}>
                          <label className="basic-data-form-label">Qual é o orçamento aproximado?</label>
                          <InputNumber
                            bordered={false}
                            className="basic-data-form-input-text"
                            formatter={currencyFormatter}
                            parser={currencyParser}
                            name="eb"
                            value={this.state.eb || ""}
                            onChange={this.onChangeNumber}
                            type="text"
                            autoComplete="off"
                            disabled={!thisFieldIsEnabled('eb')} />
                        </div>
                      </div>
                    </div>
                  </Col>
                  <Col style={{ textAlign: 'right' }} span={24}>
                    <Button onClick={this.submit} className={thisFieldIsEnabled('submit-action') ? "action-button" : "action-button inactive"} size={'large'} type={"primary"} disabled={!thisFieldIsEnabled('submit-action')}> Continuar </Button>
                  </Col>
                </Row>
              </Content>
            </div>
          </div>
        )}

      </Layout >
    );
  }
}

export default BasicCustomerForm;