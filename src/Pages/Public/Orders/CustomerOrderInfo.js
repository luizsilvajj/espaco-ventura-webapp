import React from 'react';
import { Layout, Row, Col, Button, Steps, Alert } from 'antd';

import SiteHeader from 'Pages/Elements/SiteHeader';
import 'antd/dist/antd.css';
import 'assets/styles/app.css';
import styles from 'assets/styles/styles';

import PageLoadingIndicator from 'Components/PageLoadingIndicator';

import 'moment/locale/pt-br';

import { handleResultData, currencyFormatter } from 'Utils/functions';

import ordersService from 'Services/orders';
import configurationsService from 'Services/configurations';

import { WhatsAppOutlined } from '@ant-design/icons';

import { CONFIG_COMMERCIAL_NUMBER } from 'Config/constants';


class CustomerOrderInfo extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      error: false,
      message: '',
      orderId: '',
      orderHash: '',
      orderData: [],
      customerFirstName: '',
      commercialPhoneNumber: '',
    };
    this.loadData = this.loadData.bind(this);
  }

  async componentDidMount() {
    await this.loadData();
  }

  loadData = async () => {
    //Carrega as informações do Pedido
    const { orderHash } = this.props.match.params;
    ordersService.findByHash(orderHash).then((result) => {
      let handledResult = handleResultData(result);
      if (handledResult.error == true) {
        this.setState({ error: true, message: handledResult.message, loading: false });
      } else {
        configurationsService.findBySlug(CONFIG_COMMERCIAL_NUMBER).then((phoneResult) => {
          let phoneHandledResult = handleResultData(phoneResult);
          if (phoneHandledResult.error != true) {
            this.setState({
              commercialPhoneNumber: phoneHandledResult.data.content,
            });
          }
        });

        this.setState({
          orderData: handledResult.data,
          orderId: handledResult.data.id,
          orderHash: handledResult.data.order_hash,
          loading: false
        });
      }
    });
  }


  onChange = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  close = async () => {
    const { history } = this.props;

    //redirect
    history.push('/');
  };

  redirectToWhatsapp = async () => {
    const { orderId, commercialPhoneNumber } = this.state;
    const countryPrefix = '55';
    let handledPhone = countryPrefix + commercialPhoneNumber.replace(/\(/g, '').replace(/\)/g, '').replace(/\-/g, '').replace(/\+/g,'').replace(/ /g, '');
    let textMessage = "Olá! Gostaria de falar sobre o orçamento que fiz através do aplicativo do Espaço Ventura. *Código do orçamento: " + orderId + "*";
    let url = `https://wa.me/${handledPhone}?text=${encodeURI(textMessage)}`;
    
    //redirect
    window.location = url;
  };

  render() {
    const { history } = this.props;

    const { Content } = Layout;
    const { Step } = Steps;

    const { loading, error, message, orderData, commercialPhoneNumber} = this.state;
    
    let customerFirstName = '';
    let customerName = (typeof (orderData.customer_name) === 'undefined') ? '' : orderData.customer_name;
    let explodedName = customerName.split(' ');

    if (explodedName[0]) {
      customerFirstName = explodedName[0];
    }


    return (
      <Layout className="main-content">
        {loading === true && <PageLoadingIndicator loadingText="Carregando informações..." />}
        {loading === false && error === true && <Alert className="m-b-20" message={"Erro"} description={message} type="error" showIcon />}
        {loading === false && error === false && (
          <div>
            <SiteHeader history={history} />
            <Content className="site-layout">
              <Steps style={styles.steps} size="small" current={4}>
                <Step title="Novo evento" />
                <Step title="Quanto custa" />
                <Step title="Finalize" />
              </Steps>
              <Row>
                <Col className="basic-data-intro" xl={12} lg={12} md={24} sm={24} xs={24} style={{ paddingBottom: 0 }}>
                  <h2 className="highlight">Pronto{customerFirstName ? ", " + customerFirstName : ""}!</h2>
                  <p>O orçamento do seu evento é de...</p>
                  <h2>{currencyFormatter(orderData.total_order_value)}</h2>
                  <p>Gostou? Que tal agendar uma visita? <br />Quer falar diretamente com um de nossos representantes?</p>
                  <Button onClick={this.redirectToWhatsapp} className="action-button" size={'large'} type={"primary"}> <WhatsAppOutlined style={{ fontSize: 24 }} /> Gostei! Quero falar com um representante! </Button>
                </Col>
                <Col className="basic-data-intro" xl={12} lg={12} md={24} sm={24} xs={24} style={{ paddingBottom: 0 }}>
                  <h4 className="highlight">Condições de pagamento</h4>
                  <ul style={{ listStyleType: 'none' }}>
                    <li>Entrada + pagamentos mensais</li>
                    <li>Pague com cartão de crédito, depósito, transferência ou PIX</li>
                  </ul>
                  <br />
                  <h4 className="highlight" style={{ fontSize: 20 }}>Alguns pontos de atenção</h4>
                  <ul style={{ listStyleType: 'none' }}>
                    <li>O contrato deve ser quitado até 15 dias antes da data do evento</li>
                    <li>Pagamentos realizados com cartão de crédito tem acréscimo de 5% no valor</li>
                  </ul>
                </Col>
              </Row>
            </Content>
            <div className="basic-data-content-container alt site-layout">
              <Content>
                <Row>
                  <Col style={{ textAlign: 'right' }} span={24}>
                    <Button onClick={this.close} className="action-button-cancel" size={'large'}> Sair </Button>
                  </Col>
                </Row>
              </Content>
            </div>
          </div>
        )}
      </Layout >
    );
  }
}

export default CustomerOrderInfo;