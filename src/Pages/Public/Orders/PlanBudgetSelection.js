import React from 'react';
import { Layout, Card, Row, Col, Button, Steps, Alert } from 'antd';
import SiteHeader from 'Pages/Elements/SiteHeader';
import 'antd/dist/antd.css';
import 'assets/styles/app.css';
import styles from 'assets/styles/styles';

import PageLoadingIndicator from 'Components/PageLoadingIndicator';

import { handleResultData, currencyFormatter } from 'Utils/functions';
import servicesService from 'Services/services';
import ordersService from 'Services/orders';
import orderItemsService from 'Services/orderItems';

import { getOrderId } from 'Utils/public-order';

class PlanBudgetSelection extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      error: false,
      message: '',
      data: [],
    };
    this.loadData = this.loadData.bind(this);
  }

  async componentDidMount() {
    await this.loadData();
  }

  loadData = async () => {
    const orderId = getOrderId();
    this.setState({ orderId: orderId });
    await ordersService.deleteItems(orderId);

    await servicesService.findPublicPlans().then((result) => {
      let handledResult = handleResultData(result);
      this.setState({ ...handledResult, loading: false });
    });
  }

  goToCustomizationPage = () => {
    const { history } = this.props;

    //redirect
    history.push('personalizar-evento');
  };

  submit = async (planId) => {
    const { history } = this.props;
    const {  orderId } = this.state;
    this.setState({ loading: true, loadingMessage: 'Por favor, aguarde...', formError: false, formMessage: '' });
    if (planId) {

      let requests = [];

      let postData = {
        order_id: orderId,
        service_id: planId,
      };

      requests.push(postData);

      const promises = requests.map(async (element, index) => {
        //Salva os serviços
        let postData = {
          order_id: element['order_id'],
          service_id: element['service_id'],
        };
        await orderItemsService.add(postData);
      });

      await Promise.all(promises);

      history.push('/finalizar-pedido');
    } else {
      this.setState({ loading: false, formError: true, formMessage: 'Por favor, escolha pelo menos um plano...', loadingMessage: '' });
    }
  }

  render() {
    const { history } = this.props;
    const { Content } = Layout;
    const { Step } = Steps;

    const { loading, error, message, data } = this.state;

    return (
      <Layout className="main-content">
        <SiteHeader history={history} />
        <Content className="site-layout plan-budget-main-container">
          <Steps style={styles.steps} size="small" current={1}>
            <Step title="Novo evento" />
            <Step title="Quanto custa" />
            <Step title="Finalize" />
          </Steps>
          {loading === true && <PageLoadingIndicator loadingText="Carregando informações..." />}
          {loading === false && error === true && <Alert className="m-b-20" message={"Erro"} description={message} type="error" showIcon />}
          {loading === false && error === false && (
            <Row>
              <Col className="plan-budget-intro" span={24}>
                <h2 className="highlight">Como gostaria de contratar nossos serviços?</h2>
                <p>Alugue somente o espaço ou personalize seu evento e só pague pelo que escolher :)</p>
              </Col>
              {data.map((plan) => {
                let planAttributes = null;
                try {
                  planAttributes = JSON.parse(plan.default_attributes);
                } catch (e) {
                  planAttributes = [];
                }
                return (
                  <Col key={plan.id} xl={6} lg={12} md={12} sm={24} xs={24}>
                    <Card className="plan-budget-card">
                      <div className="plan-budget-card-hightlight-container">
                        <h2 className="plan-budget-card-highlight-title">{plan.title}</h2>
                        <h3 className="plan-budget-card-highlight-price">{currencyFormatter(plan.price)}</h3>
                        <span className="plan-budget-card-highlight-description">{plan.headline}</span>
                      </div>
                      <div className="plan-budget-card-items">
                        <ul>
                          {planAttributes.map((attribute, index) => {
                            return (
                              <li key={index}>{attribute}</li>
                            );
                          })}
                        </ul>
                      </div>
                      <div className="plan-budget-card-actions">
                        <Button onClick={() => this.submit(plan.id)} className="action-button" size={'large'} type={"primary"}> Escolher esse </Button>
                      </div>
                    </Card>
                  </Col>
                )
              })}
              <Col xl={6} lg={12} md={12} sm={24} xs={24}>
                <Card className="plan-budget-card plan-bugdet-customized">
                  <div className="plan-budget-card-hightlight-container">
                    <h2 className="plan-budget-card-highlight-title">&nbsp;</h2>
                    <h3 className="plan-budget-card-highlight-price">&nbsp;</h3>
                    <span className="plan-budget-card-highlight-description">Escolha quais serviços contratar, de acordo com o que você precisa...</span>
                  </div>
                  <div className="plan-budget-card-actions">
                    <Button onClick={this.goToCustomizationPage} className="action-button" size={'large'} type={"primary"}> Personalizar </Button>
                  </div>
                </Card>
              </Col>
            </Row>
          )}

        </Content>
      </Layout>
    );
  }
}

export default PlanBudgetSelection;