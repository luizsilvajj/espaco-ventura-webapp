import React from 'react';
import { Layout, Card, Switch, Typography, Row, Col, Button, Steps, Alert } from 'antd';
import SiteHeader from 'Pages/Elements/SiteHeader';

import 'antd/dist/antd.css';
import 'assets/styles/app.css';
import styles from 'assets/styles/styles';

import PageLoadingIndicator from 'Components/PageLoadingIndicator';
import ServiceDetails from 'Components/ServiceDetails';

import { handleResultData, currencyFormatter } from 'Utils/functions';
import servicesService from 'Services/services';
import orderItemsService from 'Services/orderItems';

import { getOrderId } from 'Utils/public-order';


class CustomerCreateCustomOrder extends React.Component {


  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      orderId: '',
      error: false,
      message: '',
      formError: false,
      formMessage: '',
      loadingMessage: '',
      formMessage: '',
      data: [],
      selectedOptions: [],
      selectedItemsCosts: [],
      freeServices: [],
      serviceDetailIsVisible: false,
      serviceDetailIsLoading: false,
      serviceDetailData: [],
      serviceDetailError: false,
      serviceDetailErrorMessage: '',
    };

    this.loadData = this.loadData.bind(this);
    this.showDrawer = this.showDrawer.bind(this);
    this.closeDrawer = this.closeDrawer.bind(this);
  }

  async componentDidMount() {
    await this.loadData();
  }

  loadData = async () => {
    const orderId = getOrderId();
    this.setState({ orderId: orderId });
    await servicesService.findPublicCustom().then((result) => {
      let handledResult = handleResultData(result), freeServices = [];

      if (handledResult.data && handledResult.data.service_categories && handledResult.data.service_categories.length > 0) {
        handledResult.data.service_categories.map((category) => {
          if (category.services && category.services.length > 0) {
            category.services.map((service) => {
              if (service.is_free == true) {
                freeServices.push({ "categoryId": category.id, "serviceId": service.id, "is_free": true });
              }
            });
          }
        });
      }
      this.setState({ ...handledResult, freeServices: freeServices, loading: false });
    });
  }

  showDrawer = async (serviceId) => {

    this.setState({
      serviceDetailIsVisible: true,
      serviceDetailIsLoading: true
    }, async () => {

      let serviceDetailData = [], serviceDetailError = false, serviceDetailErrorMessage = '';

      await servicesService.findPublicById(serviceId).then((result) => {
        let handledResult = handleResultData(result);

        if (handledResult.data) {
          serviceDetailData = handledResult.data
          serviceDetailError = handledResult.error
          serviceDetailErrorMessage = handledResult.message;

        }
        this.setState({
          serviceDetailIsLoading: false,
          serviceDetailData: serviceDetailData,
          serviceDetailError: serviceDetailError,
          serviceDetailErrorMessage: serviceDetailErrorMessage,
        });
      });
    });

  };

  closeDrawer = () => {
    this.setState({
      serviceDetailIsVisible: false,
      serviceDetailData: [],
      serviceDetailError: false,
      serviceDetailIsLoading: false,
      serviceDetailErrorMessage: '',
    });
  };


  onChange = async (categoryId, serviceId, servicePrice, isChecked) => {
    let { selectedOptions, selectedItemsCosts } = this.state;
    let newSelectedOptions = [];
    let newSelectedItemsCosts = [];

    newSelectedOptions = selectedOptions.filter(element => element.categoryId != categoryId);
    newSelectedItemsCosts = selectedItemsCosts.filter(element => element.categoryId != categoryId);


    if (isChecked === true) {
      newSelectedOptions.push({ "categoryId": categoryId, "serviceId": serviceId });
      newSelectedItemsCosts.push({ "categoryId": categoryId, "serviceId": serviceId, "price": servicePrice });
    }

    this.setState({ selectedOptions: newSelectedOptions, selectedItemsCosts: newSelectedItemsCosts, formError: false, formMessage: '' });
  };

  isSelected = (categoryId, serviceId) => {
    const { selectedOptions } = this.state;
    let output = false;

    let isSelected = selectedOptions.filter(element => element.categoryId == categoryId && element.serviceId == serviceId);

    if (isSelected.length > 0) {
      output = true;
    }

    return output;
  };

  orderTotalPrice = () => {
    const { selectedItemsCosts } = this.state;
    let output = selectedItemsCosts.reduce((currentTotal, { price }) => parseFloat(currentTotal) + parseFloat(price), 0);

    return currencyFormatter(output);
  }

  submit = async () => {
    const { history } = this.props;
    const { freeServices, selectedOptions, orderId } = this.state;
    this.setState({ loading: true, loadingMessage: 'Por favor, aguarde...', formError: false, formMessage: '' });
    if (selectedOptions.length > 0) {

      let requests = [];

      for (var i = 0; i < freeServices.length; i++) {
        let postData = {
          order_id: orderId,
          service_id: freeServices[i].serviceId,
        };

        requests.push(postData);
      }


      for (var i = 0; i < selectedOptions.length; i++) {
        let postData = {
          order_id: orderId,
          service_id: selectedOptions[i].serviceId,
        };

        requests.push(postData);
      }

      const promises = requests.map(async (element, index) => {
        //Salva os serviços

        let postData = {
          order_id: element['order_id'],
          service_id: element['service_id'],
        };
        await orderItemsService.add(postData);
      });

      await Promise.all(promises);

      history.push('/finalizar-pedido');
    } else {
      this.setState({ loading: false, formError: true, formMessage: 'Por favor, escolha pelo menos um serviço...', loadingMessage: '' });
    }
  };

  render() {
    const { history } = this.props;
    const { Content } = Layout;
    const { Title } = Typography;
    const { Meta } = Card;

    const { Step } = Steps;

    const checkedText = 'Quero';
    const uncheckedText = 'Não quero';

    const { loading, error, message, data, serviceDetailIsVisible, serviceDetailIsLoading, serviceDetailData, serviceDetailError, serviceDetailErrorMessage, formError, formMessage, loadingMessage } = this.state;
    const { isSelected, orderTotalPrice, showDrawer, closeDrawer } = this;

    return (
      <Layout className="main-content">
        <SiteHeader history={history} />
        <Content className="site-layout">
          <Steps style={styles.steps} size="small" current={1}>
            <Step title="Novo evento" />
            <Step title="Quanto custa" />
            <Step title="Finalize" />
          </Steps>
        </Content>
        {loading === true && <PageLoadingIndicator loadingText={loadingMessage || "Carregando serviços..."} />}
        {loading === false && error === true && <Alert className="m-b-20" message={"Erro"} description={message} type="error" showIcon />}
        {loading === false && error === false && data.service_categories && (
          <div>
            <div className="services-container" style={{ minHeight: 380 }}>
              {loading === false && formError === true && <Content className="site-layout"><Alert className="m-b-20" message={"Erro"} description={formMessage} type="error" showIcon /></Content>}
              {data.service_categories.map((category, categoryIndex) => {
                let rowClass = (categoryIndex % 2 == 0) ? 'service-group' : 'service-group alt-row';
                return (
                  <div key={category.id} className={rowClass}>
                    <Content className="site-layout service-content">
                      <Title level={3} className="service-group-title">{category.title}</Title>
                      {category.services && category.services.length > 0 && category.services.map((service, serviceIndex) => {

                        return (
                          <Card key={service.id} hoverable headStyle={styles.serviceCardHeader} className={service.is_free == true || isSelected(category.id, service.id) == true ? "service-card active" : "service-card"}>
                            <Row>
                              <Col span={12}>
                                <Title level={4}>{service.title}</Title>
                                <Meta description={service.headline} />
                                <div className="service-card-details-link-container">
                                  <a
                                    onClick={(e) => { showDrawer(service.id); }}
                                    className="service-card-details-link">
                                    Mais detalhes
                                  </a>
                                </div>
                              </Col>
                              <Col className="service-card-action-container" span={12}>
                                {service.is_free == false && (
                                  <>
                                    <p className="service-card-investiment">{currencyFormatter(service.price)}</p>
                                    <Switch checkedChildren={checkedText} unCheckedChildren={uncheckedText} name={`category-${category.id}-service-${service.id}`} checked={isSelected(category.id, service.id)} onChange={(isChecked) => this.onChange(category.id, service.id, service.price, isChecked)} />
                                  </>
                                )}
                                {service.is_free == true && (
                                  <p className="service-card-investiment"><del>{currencyFormatter(service.price)}</del><br />incluso</p>
                                )}
                              </Col>
                            </Row>
                          </Card>
                        );
                      })}
                    </Content>
                  </div>
                );
              })}
            </div>
            <div className="services-total">
              <Row align={'middle'}>
                <Col span={12}>
                  <p className="services-total-label">Valor Total <br /><span className="services-total-label-currency"><span className="services-total-value">{orderTotalPrice()}</span></span></p>
                </Col>
                <Col style={{ textAlign: 'right' }} span={12}>
                  <Button onClick={this.submit} className="action-button" size={'large'} type={"primary"}> Continuar </Button>
                </Col>
              </Row>
            </div>
            <ServiceDetails
              isVisible={serviceDetailIsVisible}
              isLoading={serviceDetailIsLoading}
              data={serviceDetailData}
              error={serviceDetailError}
              message={serviceDetailErrorMessage}
              onClose={closeDrawer} />
          </div>
        )
        }
      </Layout>
    );
  }
}

export default CustomerCreateCustomOrder;