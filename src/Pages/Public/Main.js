import React from 'react';
import { Layout, Row, Col, Button, Avatar, Alert, Carousel } from 'antd';
import SiteHeader from 'Pages/Elements/SiteHeader';
import 'antd/dist/antd.css';
import 'assets/styles/app.css';
import calendar from 'assets/images/calendar-no-background.svg';

import { UserOutlined } from '@ant-design/icons';

import PageLoadingIndicator from 'Components/PageLoadingIndicator';

import { handleResultData } from 'Utils/functions';

import { CONFIG_COMMERCIAL_NUMBER, HOME_INDICATOR_FACEBOOK_RATING, HOME_INDICATOR_GOOGLE_COMMENTS_RATING, HOME_INDICATOR_TOTAL_EVENTS } from 'Config/constants';

import testimonialsService from 'Services/testimonials';
import configurationsService from 'Services/configurations';

class Main extends React.Component {


  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      error: false,
      message: '',
      data: [],
      totalEvents: '',
      facebookRating: '',
      googleCommentsRating: '',
    };
    this.loadData = this.loadData.bind(this);
  }

  async componentDidMount() {
    await this.loadData();
  }

  loadData = async () => {
    let totalEvents = '', facebookRating = '', googleCommentsRating = '';

    const [totalEventsResponse, facebookRatingResponse, googleCommentsRatingResponse] = await Promise.all([
      configurationsService.findBySlug(HOME_INDICATOR_TOTAL_EVENTS),
      configurationsService.findBySlug(HOME_INDICATOR_FACEBOOK_RATING),
      configurationsService.findBySlug(HOME_INDICATOR_GOOGLE_COMMENTS_RATING)
    ]);

    if (totalEventsResponse.error != true) {
      totalEvents = totalEventsResponse.data.content;
    }

    if (facebookRatingResponse.error != true) {
      facebookRating = facebookRatingResponse.data.content;
    }

    if (googleCommentsRatingResponse.error != true) {
      googleCommentsRating = googleCommentsRatingResponse.data.content;
    }

    await testimonialsService.findAll().then((result) => {
      let handledResult = handleResultData(result);
      this.setState({ ...handledResult, loading: false });
    });

    this.setState({
      totalEvents: totalEvents,
      facebookRating: facebookRating,
      googleCommentsRating: googleCommentsRating
    });
  }
  goToEventCreationPage = () => {
    const { history } = this.props;

    //redirect
    history.push('novo-evento');
  };





  render() {
    const { history } = this.props;
    const { Content } = Layout;

    const { loading, error, message, data, totalEvents, facebookRating, googleCommentsRating } = this.state;

    return (
      <Layout className="main-content">
        {loading === true && <PageLoadingIndicator loadingText="Carregando informações..." />}
        {loading === false && error === true && <Alert className="m-b-20" message={"Erro"} description={message} type="error" showIcon />}
        {loading === false && error === false && (
          <div>
            <SiteHeader history={history} light />
            <Content>
              <div className="site-layout home-main-container">
                <Row>
                  <Col lg={10} md={24} sm={24} xs={24}>
                    <div className="home-main-block">
                      <h2>Seja o produtor do seu evento</h2>
                      <p>Monte o seu evento de acordo com o orçamento disponível...</p>
                      <Button onClick={this.goToEventCreationPage} className={"action-button"} size={'large'} type={"primary"}> Começar </Button>
                    </div>
                  </Col>
                  <Col lg={14} md={24} sm={24} xs={24} className="home-main-image-container">
                    <img src={calendar} className="home-main-image" alt="Calendário" />
                  </Col>
                </Row>
              </div>
              <Row align={'middle'}>
                <Col span={24} className="home-reputation-block">
                  <div className="site-layout">
                    <h2 className="text-center">Sobre o Espaço Ventura</h2>
                    <Row>
                      <Col xl={8} lg={12} md={24} sm={24} xs={24} className="text-center">
                        <h3 className="reputation-indicator">{googleCommentsRating}<span className="reputation-indicator-scale"> / 5</span></h3>
                        <p className="reputation-description">Em comentários no Google</p>
                      </Col>
                      <Col xl={8} lg={12} md={24} sm={24} xs={24} className="text-center">
                        <h3 className="reputation-indicator">{totalEvents}</h3>
                        <p className="reputation-description">Eventos realizados com absoluto sucesso</p>
                      </Col>
                      <Col xl={8} lg={12} md={24} sm={24} xs={24} className="text-center">
                        <h3 className="reputation-indicator">{facebookRating}<span className="reputation-indicator-scale"> / 5</span></h3>
                        <p className="reputation-description">Em avaliaçãos no Facebook</p>
                      </Col>
                    </Row>
                  </div>
                </Col>
              </Row>
              <Row align={'middle'}>
                <Col span={24} className="home-testimonials-block">
                  <div className="site-layout">
                    <h2 className="text-center">O que dizem sobre a gente</h2>
                    <Row>
                      <Col span={24}>
                        <Carousel effect="fade" autoplay autoplaySpeed={7000}>
                          {data.map((testimonial) => {
                            return (
                              <div className="testimonial-item" key={testimonial.id}>
                                <div className="avatar">{
                                  testimonial.customer_picture ? <Avatar size={64}
                                    src={testimonial.customer_picture}
                                    alt={testimonial.customer_name}
                                  /> : <Avatar style={{ background: '#b2936d' }} size={64} icon={<UserOutlined />} />
                                }</div>
                                <div className="customer-name">{testimonial.customer_name}</div>
                                <div className="content">{testimonial.content}</div>
                              </div>
                            );
                          })}
                        </Carousel>
                      </Col>
                    </Row>
                  </div>
                </Col>
              </Row>
              <div className="site-layout home-call-to-action">
                <Row>
                  <Col span={24}>
                    <div className="home-call-to-action-block text-center">
                      <h2>Faça seu evento com a gente</h2>
                      <p>Do aluguel do espaço a uma festa completa... Podemos te oferecer todas as soluções de acordo com o seu orçamento...</p>
                      <Button onClick={this.goToEventCreationPage} className={"action-button"} size={'large'} type={"primary"}> Começar </Button>
                    </div>
                  </Col>
                </Row>
              </div>
            </Content>
          </div >
        )
        }
      </Layout>
    );
  }
}

export default Main;