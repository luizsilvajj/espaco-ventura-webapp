import React from 'react';
import { render } from 'react-dom';
import Routes from 'Config/routes';

import { Provider } from 'react-redux';

import store from 'Redux/store';


const App = () => {
    return (
        <Provider store={store}>
            <Routes />
        </Provider>
    );
};

render(<App />, document.getElementById('root'));