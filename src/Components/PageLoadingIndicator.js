import React from 'react';
import { Row, Col } from 'antd';
import LoadingIndicator from 'Components/LoadingIndicator';

function PageLoadingIndicator(props) {

  return (
    <Row justify={"center"}>
      <Col flex span={4} style={{ height: '70vh', display: "flex", justifyContent: "center", alignItems: "center" }} >
        <LoadingIndicator {...props} />
      </Col>
    </Row>
  );
}


export default PageLoadingIndicator;