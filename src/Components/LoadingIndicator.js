import React from 'react';
import { Spin } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';

function LoadingIndicator(props) {
  const { loadingText } = props;
  const antIcon = <LoadingOutlined style={{ fontSize: 24, color: '#b2936d' }} spin />;
  return (
      <Spin style={{color: '#b2936d'}} indicator={antIcon} tip={loadingText ? loadingText : 'Carregando informações...'} />
  );
}


export default LoadingIndicator;