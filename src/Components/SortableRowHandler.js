import { sortableContainer, sortableElement, sortableHandle } from 'react-sortable-hoc';
import { MenuOutlined } from '@ant-design/icons';


export const DragHandle = sortableHandle(() => <MenuOutlined style={{ cursor: 'grab', color: '#999' }} />);

export const SortableItem = sortableElement(props => <tr {...props} />);

export const SortableContainer = sortableContainer(props => <tbody {...props} />);

export const SortableRowHandler = {
    DragHandle,
    SortableItem,
    SortableContainer
}

export default SortableRowHandler;