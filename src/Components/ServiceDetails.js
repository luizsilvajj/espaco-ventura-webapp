
import React from 'react';
import { Drawer, Divider, Row, Col, Alert, Image } from 'antd';

import 'antd/dist/antd.css';
import 'assets/styles/app.css';

import PageLoadingIndicator from 'Components/PageLoadingIndicator';

import { EllipsisOutlined } from '@ant-design/icons';

import { currencyFormatter } from 'Utils/functions';

class ServiceDetails extends React.Component {

    constructor(props) {
        super(props);
    }

    DescriptionItem = ({ title, content }) => {
        return <div className="site-description-item-profile-wrapper">
            <p className="site-description-item-profile-p-label">{title}</p>
            <p className="site-description-item-profile-p-content">{content}</p>
        </div>
    };

    render() {
        const { DescriptionItem } = this;
        const { isVisible: visible, isLoading: loading, data, error, message, onClose } = this.props;
        return (
            <div className="drawer">
                <Drawer
                    className="general-drawer"
                    width={"80%"}
                    placement="right"
                    closable={false}
                    onClose={onClose}
                    visible={visible}
                >
                    {loading === true && <PageLoadingIndicator loadingText="Carregando detalhes do serviço..." />}
                    {loading === false && error === true && <Alert className="m-b-20" message={"Erro"} description={message} type="error" showIcon />}
                    {loading === false && error === false && data && (
                        <div className="drawer-content">
                            <Row>
                                <Col span={20}>
                                    <h3 className="service-detail-main-title"> {data.title} </h3>
                                    <p className="service-detail-headline">{data.headline}</p>
                                    <p>{data.description}</p>
                                </Col>
                                <Col span={4} style={{ overflow: "hidden" }}>
                                    <EllipsisOutlined className="service-detail-description-icon" />
                                </Col>
                            </Row>
                            <Divider />
                            <Row>
                                <Col span={12}>
                                    <DescriptionItem title="Categoria" content={data.service_category ? data.service_category.title : ""} />
                                </Col>
                                <Col span={12}>
                                    <DescriptionItem title="Valor" content={data.is_free ? <span><del>{currencyFormatter(data.price)}</del> <br />(incluso)</span> : currencyFormatter(data.price)} />
                                </Col>
                            </Row>
                            {data.service_media && data.service_media.length > 0 &&
                                <div>
                                    <Divider />
                                    <Row>
                                        <Col span={24}>
                                            <DescriptionItem title="Fotos" content="" />
                                            {data.service_media.map((element, index) => {
                                                return (
                                                    <div className="service-detail-image-container">
                                                        <Image className="service-detail-image" key={index} width={100} src={element.content_url} />
                                                    </div>
                                                );
                                            })}
                                        </Col>
                                    </Row>
                                </div>
                            }
                            <Divider />
                            <Row>
                                <Col span={24}>
                                    <DescriptionItem title="Fornecedor" content={data.service_provider ? data.service_provider.name : ""} />
                                </Col>
                                <Col span={12}>
                                    <DescriptionItem title="Site" content={data.service_provider ? (data.service_provider.website ? <a className="service-detail-link" href={`https://${data.service_provider.website}`} target="_blank">{data.service_provider.website}</a> : "") : ""} />
                                </Col>
                                <Col span={12}>
                                    <DescriptionItem title="Instagram" content={data.service_provider ? (data.service_provider.instagram ? <a className="service-detail-link" href={`https://instagram.com/${data.service_provider.instagram}`} target="_blank">{data.service_provider.instagram}</a> : "") : ""} />
                                </Col>
                            </Row>
                        </div>
                    )}
                </Drawer>
            </div>
        );
    }
}

export default ServiceDetails;
